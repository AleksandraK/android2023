package com.example.slagalicaprojekat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;


    private FirebaseUser user;
    private Boolean leaveIntent = true;

    Dialog dialog;

    String currentGameId = null;

    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton singlePlayerButton = findViewById(R.id.singlePlayerButton);
        ImageButton multiplayerButton = findViewById(R.id.multiplayerButton);
        Button loginButton = findViewById(R.id.loginButton);
        Button registerButton = findViewById(R.id.registerButton);

        mAuth = FirebaseAuth.getInstance();


        singlePlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = mAuth.getCurrentUser();
                if(user!=null) {
                    Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                    leaveIntent = false;
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent  = new Intent(getApplicationContext(), LoginActivity.class);
                    leaveIntent = false;
                    startActivity(intent);
                }
            }
        });

        multiplayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getApplicationContext(), LoginActivity.class);
                leaveIntent = false;
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getApplicationContext(), LoginActivity.class);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getApplicationContext(), RegistrationActivity.class);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

    }


    public Boolean isDataValid(String str){
        return str!=null;
    }


    public void gameInvitationListener(){
        user = mAuth.getCurrentUser();
        String userId = user.getUid();
        if(userId!=null){
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            DatabaseReference currentUserRef = usersRef.child(userId);
            DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

            ValueEventListener gameEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot gameSnapshot : dataSnapshot.getChildren()){
                        if(currentGameId == null){
                            break;
                        }
                        else{
                            String gameId = gameSnapshot.getKey();
                            if(isDataValid(gameId)){
                                if(currentGameId.equals(gameId)){
                                    if(dialog!=null){
                                        dialog.cancel();
                                    }

                                    return;

                                }
                            }else{
                                continue;
                            }
                        }
                    }

                    for (DataSnapshot gameSnapshot : dataSnapshot.getChildren()) {

                        String player1Id = gameSnapshot.child("player1").getValue(String.class);
                        String player2Id = gameSnapshot.child("player2").getValue(String.class);
                        String status = gameSnapshot.child("status").getValue(String.class);
                        String gameId = gameSnapshot.getKey();
                        currentGameId = gameId;

                        if(isDataValid(player1Id) && isDataValid(player2Id) && isDataValid(gameId) && isDataValid(status)){

                            if (player2Id.equals(userId) && status.equals("pending")) {

                                DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users");
                                DatabaseReference currentGame = gameRef.child("games").child(gameId);
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle("Позив")
                                        .setMessage("Позвани сте у игру!")
                                        .setPositiveButton("Прихвати", (dialog, which) -> {


                                            currentGame.child("status").setValue("accepted")
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {

                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                                            intent.putExtra("IS_INVITED", true);
                                                            intent.putExtra("GAME_ID", gameId);
                                                            leaveIntent = false;
                                                            startActivity(intent);
                                                            finish();
                                                            if (!task.isSuccessful()) {
                                                                Toast.makeText(MainActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                        })
                                        .setNegativeButton("Одбиј", (dialog, which) -> {


                                            currentGame.child("status").setValue("declined")
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (!task.isSuccessful()) {
                                                                Toast.makeText(MainActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                        });
                                dialog = builder.create();
                                dialog.show();


                                break;
                            }
                        }


                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //Toast.makeText(RegistratedUserMain.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
                }
            };
            listenerManager.addValueEventListener(gamesReference, gameEventListener);

        }


    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null && !currentUser.isAnonymous()){
            Intent intent = new Intent(MainActivity.this, RegistratedUserMain.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        } else if(currentUser!=null && currentUser.isAnonymous()){
            gameInvitationListener();
        }else if(currentUser==null){
            mAuth.signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Authentication successful, get the user object
                                FirebaseUser user = mAuth.getCurrentUser();
                                if (user != null) {
                                    String userId = user.getUid();
                                    DatabaseReference activeUsersRef = FirebaseDatabase.getInstance().getReference("users").child("active");
                                    activeUsersRef.child(userId).setValue(true);
                                    gameInvitationListener();
//                                Toast.makeText(MainActivity.this, userId, Toast.LENGTH_SHORT).show();
                                } else {
                                    // Handle user being null if unexpected
                                }
                            } else {
                                // Handle authentication failure
//                            Toast.makeText(MainActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        user = mAuth.getCurrentUser();
        if (user != null && user.isAnonymous()) {
            String userId = user.getUid();
            DatabaseReference activeUsersRef = FirebaseDatabase.getInstance().getReference("users").child("active");
            activeUsersRef.child(userId).setValue(null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        user = mAuth.getCurrentUser();
        if (user != null && user.isAnonymous()) {
            String userId = user.getUid();
            DatabaseReference activeUsersRef = FirebaseDatabase.getInstance().getReference("users").child("active");
            activeUsersRef.child(userId).setValue(true);
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.closingTitle)
                .setMessage(R.string.closingDescription)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    @Override
    protected void onDestroy() {

        if(leaveIntent == true){
            FirebaseAuth auth;
            FirebaseUser user;
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            if(user!=null && !user.isAnonymous()){
                String userId = user.getUid();
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                DatabaseReference activeUsers = usersRef.child("active");
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }

            if(user!=null && user.isAnonymous()){
                user.delete()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                } else {
                                }
                            }
                        });
            }

        }
        listenerManager.removeAllListeners();
        super.onDestroy();
    }


}
