package com.example.slagalicaprojekat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {

    FirebaseAuth auth;

    FirebaseUser user;


    String currentGameId = null;

    Dialog dialog;

    private Boolean leaveIntent = true;

    private ImageButton backButton;

    private Button logoutButton;

    private TextView userEmail;
    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();

    private TextView username;

    private ImageButton pickImageButton;
    private static final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        backButton = findViewById(R.id.backButton);
        logoutButton = findViewById(R.id.logoutButton);
        userEmail = findViewById(R.id.email);
        username = findViewById(R.id.username);
        pickImageButton = findViewById(R.id.changeImageButton);
        auth = FirebaseAuth.getInstance();

        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

        user = auth.getCurrentUser();
        if(user == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }else{
            userEmail.setText(user.getEmail());
            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference currentUserRef = usersRef.child(userId);
            ValueEventListener usernameListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String usernameValue = dataSnapshot.getValue(String.class);
                        username.setText(usernameValue);
                    }else{
                        username.setText(R.string.errorWithGettingUsername);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    username.setText(R.string.databaseError);
                }
            };

            listenerManager.addValueEventListener(currentUserRef.child("username"), usernameListener);

            ValueEventListener profileimgListener = new ValueEventListener() {


                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userImageRef = dataSnapshot.getValue(String.class);
                        ImageView imageView = findViewById(R.id.profileImage);
                        Picasso.get().load(String.valueOf(userImageRef)).into(imageView);
                    }else{
//                        Toast.makeText(ProfileActivity.this, R.string.errorWithGettingProfileImg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(ProfileActivity.this, R.string.errorWithGettingProfileImg, Toast.LENGTH_SHORT).show();
                }
            };

            listenerManager.addValueEventListener(currentUserRef.child("profileimg"), profileimgListener);

        }

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth auth;
                FirebaseUser user;
                auth = FirebaseAuth.getInstance();
                user = auth.getCurrentUser();

                if(user!=null){
                    String userId = user.getUid();
                    DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                    DatabaseReference activeUsers = usersRef.child("active");
                    activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(ProfileActivity.this, R.string.successfulLogOutMsg, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

        pickImageButton.setOnClickListener(view -> openImagePicker());
        gameInvitationListener();
        addInvitationListener();
    }

    private void addInvitationListener(){
        DatabaseReference currentUser = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations");

        ChildEventListener newInvListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String invId = snapshot.getValue(String.class);
                DatabaseReference currentUserRef = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
                DatabaseReference currentInv = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations").child(snapshot.getKey());
                DatabaseReference invUserRef = FirebaseDatabase.getInstance().getReference("users").child(invId);
                invUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            final String invUserName = snapshot.child("username").getValue(String.class);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                            builder.setTitle("Захтев")
                                    .setMessage("Корисник " + invUserName + " вам је послао захтев за пријатељство.")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {
                                        invUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(user.getUid())){
                                                            return;
                                                        }
                                                    }
                                                    invUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(user.getUid());
                                                    Toast.makeText(ProfileActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    invUserRef.child("friends").child("1").setValue(user.getUid());
                                                    Toast.makeText(ProfileActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        currentUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(invId)){
                                                            return;
                                                        }
                                                    }
                                                    currentUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(invId);
                                                    Toast.makeText(ProfileActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    currentUserRef.child("friends").child("1").setValue(invId);
                                                    Toast.makeText(ProfileActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });
                                        currentInv.removeValue();


                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {
                                        currentInv.removeValue();
                                        Toast.makeText(ProfileActivity.this, "Захтев одбијен!", Toast.LENGTH_SHORT).show();


                                    });
                            dialog = builder.create();
                            dialog.show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addChildEventListener(currentUser, newInvListener);

    }


    private void openImagePicker() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            ImageView imageView = findViewById(R.id.profileImage);
            imageView.setImageURI(imageUri);
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            StorageReference userImageRef = storageRef.child("users/" + userId + "/profile.jpg");
            UploadTask uploadTask = userImageRef.putFile(imageUri);
            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
            builder.setCancelable(false);
            builder.setView(R.layout.progress_layout);
            AlertDialog dialog = builder.create();
            dialog.show();

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    userImageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            dialog.dismiss();
                            String imageUrl = uri.toString();
                            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference currentUserRef = usersRef.child(userId);
                            currentUserRef.child("profileimg").setValue(imageUrl)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(ProfileActivity.this,R.string.errorWithSettingProfileImg , Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(ProfileActivity.this,exception.toString() , Toast.LENGTH_SHORT).show();
                }
            });


        }
    }

    public void gameInvitationListener(){
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        DatabaseReference currentUserRef = usersRef.child(userId);
        DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

        ValueEventListener gameEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot gameSnapshot : dataSnapshot.getChildren()){
                    if(currentGameId == null){
                        break;
                    }
                    else{
                        String gameId = gameSnapshot.getKey();
                        if(isDataValid(gameId)){
                            if(currentGameId.equals(gameId)){
                                if(dialog!=null){
                                    dialog.cancel();
                                }

                                return;

                            }
                        }else{
                            continue;
                        }
                    }
                }

                for (DataSnapshot gameSnapshot : dataSnapshot.getChildren()) {

                    String player1Id = gameSnapshot.child("player1").getValue(String.class);
                    String player2Id = gameSnapshot.child("player2").getValue(String.class);
                    String status = gameSnapshot.child("status").getValue(String.class);
                    String gameId = gameSnapshot.getKey();
                    currentGameId = gameId;

                    if(isDataValid(player1Id) && isDataValid(player2Id) && isDataValid(gameId) && isDataValid(status)){

                        if (player2Id.equals(userId) && status.equals("pending")) {

                            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference currentGame = gameRef.child("games").child(gameId);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                            builder.setTitle("Позив")
                                    .setMessage("Позвани сте у игру!")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {


                                        currentGame.child("status").setValue("accepted")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {

                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                                        intent.putExtra("IS_INVITED", true);
                                                        intent.putExtra("GAME_ID", gameId);
                                                        leaveIntent = false;
                                                        startActivity(intent);
                                                        finish();
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(ProfileActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {


                                        currentGame.child("status").setValue("declined")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(ProfileActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    });
                            dialog = builder.create();
                            dialog.show();


                            break;
                        }
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(ProfileActivity.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
            }
        };


        listenerManager.addValueEventListener(gamesReference, gameEventListener);
    }

    public Boolean isDataValid(String str){
        return str!=null;
    }


    @Override
    public void onBackPressed() {
        leaveIntent = false;
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    protected void onDestroy() {

        if(leaveIntent == true){
            FirebaseAuth auth;
            FirebaseUser user;
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            if(user!=null){
                String userId = user.getUid();
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                DatabaseReference activeUsers = usersRef.child("active");
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }

        }

        listenerManager.removeAllListeners();
        super.onDestroy();
    }

}