package com.example.slagalicaprojekat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEditText, passwordEditText;
    private Button loginButton;
    private Button goOnRegisterPage;
    private ImageButton backButton;
    private FirebaseAuth mAuth;

    private Boolean leaveIntent = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailEditText = findViewById(R.id.email);
        passwordEditText = findViewById(R.id.password);
        loginButton = findViewById(R.id.loginButton);
        backButton = findViewById(R.id.backButton);
        goOnRegisterPage = findViewById(R.id.goOnRegisterPage);

        mAuth = FirebaseAuth.getInstance();


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailOrUsername = emailEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString();
                if (TextUtils.isEmpty(emailOrUsername)) {
                    emailEditText.setError(getResources().getString(R.string.enterEmailMsg));
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    passwordEditText.setError(getResources().getString(R.string.enterPasswordMsg));
                    return;
                }


                FirebaseUser currentUser = mAuth.getCurrentUser();
                if(currentUser!=null && currentUser.isAnonymous()){
                    currentUser.delete()
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                    } else {
                                    }
                                }
                            });
                }

                mAuth.signInWithEmailAndPassword(emailOrUsername, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(LoginActivity.this, R.string.successfulLoginMsg, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(LoginActivity.this, RegistratedUserMain.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    leaveIntent = false;
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, R.string.unsuccessfulLoginMsg, Toast.LENGTH_SHORT).show();


                                }
                            }
                        });

            }
        });

        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

        goOnRegisterPage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null && !currentUser.isAnonymous()){
            Toast.makeText(LoginActivity.this, R.string.alreadyLoggedInMsg, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(LoginActivity.this, RegistratedUserMain.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        leaveIntent = false;
        Intent intent  = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }



    @Override
    protected void onDestroy() {

        if(leaveIntent == true){
            FirebaseAuth auth;
            FirebaseUser user;
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            if(user!=null){
                String userId = user.getUid();
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                DatabaseReference activeUsers = usersRef.child("active");
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }

        }
        super.onDestroy();
    }


}
