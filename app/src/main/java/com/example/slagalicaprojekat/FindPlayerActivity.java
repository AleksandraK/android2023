package com.example.slagalicaprojekat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class FindPlayerActivity extends AppCompatActivity {


    private LinearLayout playerListLayout;
    FirebaseAuth auth;

    FirebaseUser user;

    Button leaveButton;

    private Boolean leaveIntent = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_player);

        playerListLayout = findViewById(R.id.playerListView);

        leaveButton = findViewById(R.id.leaveButton);

        auth = FirebaseAuth.getInstance();

        user = auth.getCurrentUser();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");

        activeUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot user : dataSnapshot.getChildren()) {
                    String currentUserId = user.getKey();
                    DatabaseReference currentUserRef = usersRef.child(currentUserId);
                    if (user.getValue(Boolean.class) == true && !currentUserId.equals(userId)) {
                        
                        addPlayer(currentUserId);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        leaveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });
    }



    private void addPlayer(String userId) {
        View playerItemView = LayoutInflater.from(this).inflate(R.layout.player_item, null);
        ImageView profilePicture = playerItemView.findViewById(R.id.profilePicture);
        TextView username = playerItemView.findViewById(R.id.username);
        Button playButton = playerItemView.findViewById(R.id.playButton);
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference currentUserRef = usersRef.child(userId);

        currentUserRef.child("profileimg").addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String userImageRef = dataSnapshot.getValue(String.class);
                    ImageView imageView = findViewById(R.id.profile);
                    Picasso.get().load(String.valueOf(userImageRef)).into(profilePicture);
                }else{
//                    Toast.makeText(FindPlayerActivity.this, R.string.errorWithGettingProfileImg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(FindPlayerActivity.this, R.string.errorWithGettingProfileImg, Toast.LENGTH_SHORT).show();
            }
        });
        currentUserRef.child("username").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String usernameValue = dataSnapshot.getValue(String.class);
                    username.setText(usernameValue);
                }else{
                    username.setText(R.string.errorWithGettingUsername);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                username.setText(R.string.databaseError);
            }
        });
        playButton.setOnClickListener(v -> {
            // Handle play button click here
        });

        playerListLayout.addView(playerItemView);

        currentUserRef.child("friends").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String usernameValue = dataSnapshot.getValue(String.class);
                    username.setText(usernameValue);
                }else{
                    username.setText(R.string.errorWithGettingUsername);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                username.setText(R.string.databaseError);
            }
        });
    }


    @Override
    public void onBackPressed() {
        leaveIntent = false;
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }



    @Override
    protected void onDestroy() {

        if(leaveIntent == true){
            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                }
            });

        }
        super.onDestroy();
    }



}