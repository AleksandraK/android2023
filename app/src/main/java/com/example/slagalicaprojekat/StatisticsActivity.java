package com.example.slagalicaprojekat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.Nullable;


public class StatisticsActivity extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseUser user;
    String userId = null;
    private ImageButton backButton;
    TextView playedGamesText, koznaznaPointsText, spojnicePointsText, asocijacijePointsText, skockoPointsText, korakpokorakPointsText, mojbrojPointsText;
    TextView winText, loseText;

    Dialog dialog;

    public String currentGameId = null;

    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();

    private  Boolean leaveIntent = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        backButton = findViewById(R.id.backButton);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();

        playedGamesText = findViewById(R.id.playedGames);
        koznaznaPointsText = findViewById(R.id.koznaznaPoints);
        spojnicePointsText = findViewById(R.id.spojnicePoints);
        asocijacijePointsText = findViewById(R.id.asocijacijePoints);
        skockoPointsText = findViewById(R.id.skockoPoints);
        korakpokorakPointsText = findViewById(R.id.korakpokorakPoints);
        mojbrojPointsText = findViewById(R.id.mojbrojPoints);

        winText = findViewById(R.id.win);
        loseText = findViewById(R.id.lose);

        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference currentUserRef = usersRef.child(userId);

        currentUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    Integer playedGames = snapshot.child("playedGames").getValue(Integer.class);
                    Integer koznaznaPoints = snapshot.child("koznaznaPoints").getValue(Integer.class);
                    Integer spojnicePoints = snapshot.child("spojnicePoints").getValue(Integer.class);
                    Integer asocijacijePoints = snapshot.child("asocijacijePoints").getValue(Integer.class);
                    Integer skockoPoints = snapshot.child("skockoPoints").getValue(Integer.class);
                    Integer korakpokorakPoints = snapshot.child("korakpokorakPoints").getValue(Integer.class);
                    Integer mojbrojPoints = snapshot.child("mojbrojPoints").getValue(Integer.class);

                    Integer win = snapshot.child("win").getValue(Integer.class);
                    Integer lose = snapshot.child("lose").getValue(Integer.class);

                    if(playedGames!=null){
                        playedGamesText.setText(String.valueOf(playedGames));
                        koznaznaPointsText.setText(String.valueOf(koznaznaPoints/playedGames));
                        spojnicePointsText.setText(String.valueOf(spojnicePoints/playedGames));
                        asocijacijePointsText.setText(String.valueOf(asocijacijePoints/playedGames));
                        skockoPointsText.setText(String.valueOf(skockoPoints/playedGames));
                        korakpokorakPointsText.setText(String.valueOf(korakpokorakPoints/playedGames));
                        mojbrojPointsText.setText(String.valueOf(mojbrojPoints/playedGames));

                        double winPercentage = (double) win / playedGames * 100;
                        double losePercentage = (double) lose / playedGames * 100;
                        winText.setText(String.format("%.2f%%", winPercentage));
                        loseText.setText(String.format("%.2f%%", losePercentage));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });
        gameInvitationListener();
        addInvitationListener();
    }

    private void addInvitationListener(){
        DatabaseReference currentUser = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations");

        ChildEventListener newInvListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String invId = snapshot.getValue(String.class);
                DatabaseReference currentUserRef = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
                DatabaseReference currentInv = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations").child(snapshot.getKey());
                DatabaseReference invUserRef = FirebaseDatabase.getInstance().getReference("users").child(invId);
                invUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            final String invUserName = snapshot.child("username").getValue(String.class);
                            AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsActivity.this);
                            builder.setTitle("Захтев")
                                    .setMessage("Корисник " + invUserName + " вам је послао захтев за пријатељство.")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {
                                        invUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(user.getUid())){
                                                            return;
                                                        }
                                                    }
                                                    invUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(user.getUid());
                                                    Toast.makeText(StatisticsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    invUserRef.child("friends").child("1").setValue(user.getUid());
                                                    Toast.makeText(StatisticsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        currentUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(invId)){
                                                            return;
                                                        }
                                                    }
                                                    currentUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(invId);
                                                    Toast.makeText(StatisticsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    currentUserRef.child("friends").child("1").setValue(invId);
                                                    Toast.makeText(StatisticsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });
                                        currentInv.removeValue();


                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {
                                        currentInv.removeValue();
                                        Toast.makeText(StatisticsActivity.this, "Захтев одбијен!", Toast.LENGTH_SHORT).show();


                                    });
                            dialog = builder.create();
                            dialog.show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addChildEventListener(currentUser, newInvListener);

    }



    public void gameInvitationListener(){
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        DatabaseReference currentUserRef = usersRef.child(userId);
        DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

        ValueEventListener gameEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot gameSnapshot : dataSnapshot.getChildren()){
                    if(currentGameId == null){
                        break;
                    }
                    else{
                        String gameId = gameSnapshot.getKey();
                        if(isDataValid(gameId)){
                            if(currentGameId.equals(gameId)){
                                if(dialog!=null){
                                    dialog.cancel();
                                }

                                return;

                            }
                        }else{
                            continue;
                        }
                    }
                }

                for (DataSnapshot gameSnapshot : dataSnapshot.getChildren()) {

                    String player1Id = gameSnapshot.child("player1").getValue(String.class);
                    String player2Id = gameSnapshot.child("player2").getValue(String.class);
                    String status = gameSnapshot.child("status").getValue(String.class);
                    String gameId = gameSnapshot.getKey();
                    currentGameId = gameId;

                    if(isDataValid(player1Id) && isDataValid(player2Id) && isDataValid(gameId) && isDataValid(status)){

                        if (player2Id.equals(userId) && status.equals("pending")) {

                            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference currentGame = gameRef.child("games").child(gameId);
                            AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsActivity.this);
                            builder.setTitle("Позив")
                                    .setMessage("Позвани сте у игру!")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {


                                        currentGame.child("status").setValue("accepted")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {

                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                                        intent.putExtra("IS_INVITED", true);
                                                        intent.putExtra("GAME_ID", gameId);
                                                        leaveIntent = false;
                                                        startActivity(intent);
                                                        finish();
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(StatisticsActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {


                                        currentGame.child("status").setValue("declined")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(StatisticsActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    });
                            dialog = builder.create();
                            dialog.show();


                            break;
                        }
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Toast.makeText(StatisticsActivity.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
            }
        };


        listenerManager.addValueEventListener(gamesReference, gameEventListener);
    }

    public Boolean isDataValid(String str){
        return str!=null;
    }


    @Override
    public void onBackPressed() {
        leaveIntent = false;
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        super.onPause();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    protected void onDestroy() {

        if(leaveIntent == true){
            FirebaseAuth auth;
            FirebaseUser user;
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            if(user!=null){
                String userId = user.getUid();
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                DatabaseReference activeUsers = usersRef.child("active");
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }

        }
        super.onDestroy();
    }

}