package com.example.slagalicaprojekat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import com.example.slagalicaprojekat.FirebaseListenerManager;

import java.util.Calendar;
import java.util.Date;
import java.util.EventListener;

public class RegistratedUserMain extends AppCompatActivity {

    FirebaseAuth auth;
    Button button, friendsButton, rangButton;

    ImageButton profileButton, statsButton;
    FirebaseUser user;

    TextView tokens, stars;

    String currentGameId = null;

    Dialog dialog;
    private static final String PREFS_NAME = "MyPrefs";
    private static final String DIALOG_DISPLAYED_KEY = "dialogDisplayed";
    private static final String LAST_LOGIN_DATE_KEY = "lastLoginDate";
    private static final String TOKEN_COUNT_KEY = "tokenCount";


    private Boolean leaveIntent = true;

    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrated_user_main);

        ImageButton singlePlayerButton = findViewById(R.id.singlePlayerButton);
        ImageButton multiplayerButton = findViewById(R.id.multiplayerButton);

        auth = FirebaseAuth.getInstance();
        button = findViewById(R.id.logoutbutton);
        profileButton = findViewById(R.id.profile);
        statsButton = findViewById(R.id.statistic);
        friendsButton = findViewById(R.id.friendsButton);
        rangButton = findViewById(R.id.rangListButton);
        tokens = findViewById(R.id.tokenss);
        stars = findViewById(R.id.starss);

        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean isDialogDisplayed = sharedPreferences.getBoolean(DIALOG_DISPLAYED_KEY, false);
        Date lastLoginDate = new Date(sharedPreferences.getLong(LAST_LOGIN_DATE_KEY, 0));
        int tokenCount = sharedPreferences.getInt(TOKEN_COUNT_KEY, 0);

        Calendar today = Calendar.getInstance();
        Calendar lastLoginCalendar = Calendar.getInstance();
        lastLoginCalendar.setTime(lastLoginDate);

        boolean backFromGamesActivity = getIntent().getBooleanExtra("NO_ACTIVE_USER", false);

        if(backFromGamesActivity){
            Toast.makeText(RegistratedUserMain.this, "Нема активних играча!", Toast.LENGTH_SHORT).show();

        }

        user = auth.getCurrentUser();
        if (user == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }else {
            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            DatabaseReference currentUserRef = usersRef.child(userId);
            activeUsers.child(userId).setValue(true)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(RegistratedUserMain.this, R.string.errorWithSettingUsername, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            ValueEventListener profileImgEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userImageRef = dataSnapshot.getValue(String.class);
                        ImageView imageView = findViewById(R.id.profile);
                        Picasso.get().load(String.valueOf(userImageRef)).into(imageView);
                    } else {

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //Toast.makeText(RegistratedUserMain.this, R.string.errorWithGettingProfileImg, Toast.LENGTH_SHORT).show();
                }
            };



            listenerManager.addValueEventListener(currentUserRef.child("profileimg"), profileImgEventListener);

            ValueEventListener tokensEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Object value = dataSnapshot.getValue();
                        if (value instanceof Long) {
                            Long longValue = (Long) value;
                            tokens.setText(String.valueOf(longValue));
                        } else if (value instanceof String) {
                            String stringValue = (String) value;
                            tokens.setText(String.valueOf(stringValue));
                        }
                    } else {
                        tokens.setText("-");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //Toast.makeText(RegistratedUserMain.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
                }
            };



            listenerManager.addValueEventListener(currentUserRef.child("tokens"), tokensEventListener);

            ValueEventListener starsEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Object value = dataSnapshot.getValue();
                        if (value instanceof Long) {
                            Long longValue = (Long) value;
                            stars.setText(String.valueOf(longValue));
                        } else if (value instanceof String) {
                            String stringValue = (String) value;
                            stars.setText(String.valueOf(stringValue));
                        }
                    } else {
                        stars.setText("-");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //Toast.makeText(RegistratedUserMain.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
                }
            };

            listenerManager.addValueEventListener(currentUserRef.child("stars"), starsEventListener);

            gameInvitationListener();



            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userId = user.getUid();
                    DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                    DatabaseReference activeUsers = usersRef.child("active");
                    DatabaseReference currentUserRef = usersRef.child(userId);
                    activeUsers.child(userId).setValue(false)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });


                    FirebaseAuth.getInstance().signOut();
                    Toast.makeText(RegistratedUserMain.this, R.string.successfulLogOutMsg, Toast.LENGTH_SHORT).show();
                    listenerManager.removeAllListeners();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    leaveIntent = false;
                    startActivity(intent);
                    finish();
                }
            });

            profileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                    leaveIntent = false;
                    startActivity(intent);
                }
            });

            statsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                    leaveIntent = false;
                    startActivity(intent);
                }
            });

            friendsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), FriendsActivity.class);
                    leaveIntent = false;
                    startActivity(intent);
                    finish();
                }
            });

            rangButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), RankActivity.class);
                    leaveIntent = false;
                    startActivity(intent);
                }
            });

            singlePlayerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    currentUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                Long tokens = snapshot.child("tokens").getValue(Long.class);
                                if(tokens>0){
                                    Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                    leaveIntent = false;
                                    startActivity(intent);
                                    finish();
                                }else{
                                    Toast.makeText(RegistratedUserMain.this, "Немате довољно токена", Toast.LENGTH_SHORT).show();

                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });



                }
            });

            multiplayerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                Long tokens = snapshot.child("tokens").getValue(Long.class);
                                if(tokens>0){
                                    Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                    leaveIntent = false;
                                    startActivity(intent);
                                    finish();
                                }else{
                                    Toast.makeText(RegistratedUserMain.this, "Немате довољно токена", Toast.LENGTH_SHORT).show();

                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            });
        }



        if (!isDialogDisplayed) {
            boolean isRegistrationSuccessful = true;

            if (isRegistrationSuccessful) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Čestitamo!")
                        .setMessage("Registracijom ste osvojili 5 tokena.")
                        .setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(DIALOG_DISPLAYED_KEY, true);
                editor.apply();
            }
        }
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        String userId = user.getUid();
        DatabaseReference currentUserRef = usersRef.child(userId);


        if (today.get(Calendar.YEAR) > lastLoginCalendar.get(Calendar.YEAR) ||
                today.get(Calendar.DAY_OF_YEAR) > lastLoginCalendar.get(Calendar.DAY_OF_YEAR)) {

            currentUserRef.child("tokens").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        Object value = snapshot.getValue();
                        if (value instanceof Long) {
                            Long longValue = (Long) value;
                            longValue += 5;
                            currentUserRef.child("tokens").setValue(longValue);
                        } else if (value instanceof String) {
                            String stringValue = (String) value;
                            Long longValue = Long.parseLong(stringValue);
                            longValue += 5;
                            stringValue = Long.toString(longValue);
                            currentUserRef.child("tokens").setValue(stringValue);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Dobrodošli!")
                    .setMessage("Dobili ste 5 tokena!")
                    .setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(TOKEN_COUNT_KEY, tokenCount);
            editor.putLong(LAST_LOGIN_DATE_KEY, today.getTimeInMillis());
            editor.apply();
        }

        addInvitationListener();

    }

    private void addInvitationListener(){
        DatabaseReference currentUser = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations");

        ChildEventListener newInvListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String invId = snapshot.getValue(String.class);
                DatabaseReference currentUserRef = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
                DatabaseReference currentInv = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations").child(snapshot.getKey());
                DatabaseReference invUserRef = FirebaseDatabase.getInstance().getReference("users").child(invId);
                invUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            final String invUserName = snapshot.child("username").getValue(String.class);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegistratedUserMain.this);
                            builder.setTitle("Захтев")
                                    .setMessage("Корисник " + invUserName + " вам је послао захтев за пријатељство.")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {
                                        invUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(user.getUid())){
                                                            return;
                                                        }
                                                    }
                                                    invUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(user.getUid());
                                                    Toast.makeText(RegistratedUserMain.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    invUserRef.child("friends").child("1").setValue(user.getUid());
                                                    Toast.makeText(RegistratedUserMain.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        currentUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(invId)){
                                                            return;
                                                        }
                                                    }
                                                    currentUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(invId);
                                                    Toast.makeText(RegistratedUserMain.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    currentUserRef.child("friends").child("1").setValue(invId);
                                                    Toast.makeText(RegistratedUserMain.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });
                                        currentInv.removeValue();


                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {
                                        currentInv.removeValue();
                                        Toast.makeText(RegistratedUserMain.this, "Захтев одбијен!", Toast.LENGTH_SHORT).show();


                                    });
                            dialog = builder.create();
                            dialog.show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addChildEventListener(currentUser, newInvListener);

    }


    public void gameInvitationListener(){
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        DatabaseReference currentUserRef = usersRef.child(userId);
        DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

        ValueEventListener gameEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot gameSnapshot : dataSnapshot.getChildren()){
                    if(currentGameId == null){
                        break;
                    }
                    else{
                        String gameId = gameSnapshot.getKey();
                        if(isDataValid(gameId)){
                            if(currentGameId.equals(gameId)){
                                if(dialog!=null){
                                    dialog.cancel();
                                }

                                return;

                            }
                        }else{
                            continue;
                        }
                    }
                }

                for (DataSnapshot gameSnapshot : dataSnapshot.getChildren()) {

                    String player1Id = gameSnapshot.child("player1").getValue(String.class);
                    String player2Id = gameSnapshot.child("player2").getValue(String.class);
                    String status = gameSnapshot.child("status").getValue(String.class);
                    String gameId = gameSnapshot.getKey();
                    currentGameId = gameId;

                    if(isDataValid(player1Id) && isDataValid(player2Id) && isDataValid(gameId) && isDataValid(status)){

                        if (player2Id.equals(userId) && status.equals("pending")) {

                            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference currentGame = gameRef.child("games").child(gameId);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegistratedUserMain.this);
                            builder.setTitle("Позив")
                                    .setMessage("Позвани сте у игру!")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {


                                        currentGame.child("status").setValue("accepted")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {

                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                                        intent.putExtra("IS_INVITED", true);
                                                        intent.putExtra("GAME_ID", gameId);
                                                        leaveIntent = false;
                                                        startActivity(intent);
                                                        finish();
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RegistratedUserMain.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {


                                        currentGame.child("status").setValue("declined")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RegistratedUserMain.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    });
                            dialog = builder.create();
                            dialog.show();


                            break;
                        }
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Toast.makeText(RegistratedUserMain.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
            }
        };


        listenerManager.addValueEventListener(gamesReference, gameEventListener);
    }

    public Boolean isDataValid(String str){
        return str!=null;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.closingTitle)
                .setMessage(R.string.closingDescription)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseUser currentUser = auth.getCurrentUser();
                        if(currentUser != null){
                            String userId = user.getUid();
                            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference activeUsers = usersRef.child("active");
                            DatabaseReference currentUserRef = usersRef.child(userId);
                            activeUsers.child(userId).setValue(false)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                        }
                                    });
                        }
                        finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }


    @Override
    protected void onPause() {
        super.onPause();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }




    protected void onDestroy() {

        if(leaveIntent == true){

            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            });

            }
        listenerManager.removeAllListeners();
        super.onDestroy();
    }



}