package com.example.slagalicaprojekat;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Dialog;
import android.app.ProgressDialog;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class AsocijacijeActivity extends AppCompatActivity {

    Handler handler = new Handler();
    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();
    private TextView timer;
    String fieldContent = "Greska";
    FirebaseAuth auth;
    FirebaseUser user;
    String userId = null;
    String gameId = null;
    String hostId = null;
    DatabaseReference gameReference;
    DatabaseReference asocijacijeReference;
    String playerValue = null;
    Dialog dialog;
    public int activeQuestion;
    private ArrayList<Integer> randomNumbers;
    private final int NUMBER_OF_QUESTIONS = 3;
    private DatabaseReference databaseReference;
    private Button[][] fieldButtons = new Button[4][4];
    private ImageButton AButton, BButton, CButton, DButton, finalButton;
    private TextInputEditText finalSolution, solutionA, solutionB, solutionC, solutionD;
    private Boolean leaveIntent = true;
    private String[] currentCorrectColumnSolutions = new String[4];
    private String currentCorrectFinalSolution = "";
    private int openedFieldsCount = 0;
    private ViewGroup rootLayout;
    private View overlayView;
    private Boolean isSecond;
    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asocijacije);

        gameId = getIntent().getStringExtra("GAME_ID");
        dialog = ProgressDialog.show(AsocijacijeActivity.this, "",
                "Чекање противника да уђе у игру.", true);
        dialog.show();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                Toast.makeText(AsocijacijeActivity.this, "Прошло је време за чекање играча.", Toast.LENGTH_SHORT).show();
            }
        };

        handler.postDelayed(runnable, 10000);

        gameId = getIntent().getStringExtra("GAME_ID");
        isSecond = getIntent().getBooleanExtra("SECOND_GAME",false);

        timer = findViewById(R.id.timer);
        gameReference= FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
        asocijacijeReference = gameReference.child("asocijacijeGame");
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();


        gameReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    hostId = snapshot.child("player1").getValue(String.class);
                    if(hostId.equals(userId)){
                        gameReference.child("player1Location").setValue("Asocijacije")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });


                        prepareGame();
                        determinePlayer("player1");

                        ValueEventListener locationListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String location = dataSnapshot.getValue(String.class);
                                    if(location.equals("Asocijacije")){
                                        startGameListener();
                                        gameReference.child("player2Location").removeEventListener(this);

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };

                        listenerManager.addValueEventListener(gameReference.child("player2Location"),locationListener);


                    }
                    else{

                        gameReference.child("player2Location").setValue("Asocijacije")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                        determinePlayer("player2");
                        ValueEventListener locationListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String location = dataSnapshot.getValue(String.class);
                                    if(location.equals("Asocijacije")){
                                        preparedGameListener();
                                        gameReference.child("player1Location").removeEventListener(this);

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };

                        listenerManager.addValueEventListener(gameReference.child("player1Location"),locationListener);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference("asocijacije");

        initializeUI();

        setButtonListeners();

        updateData();

        setTextFieldsListeners();

    }


    public void layoutOn(){
        rootLayout = findViewById(android.R.id.content);

        LayoutInflater inflater = LayoutInflater.from(this);
        overlayView = inflater.inflate(R.layout.overlay_layout, rootLayout, false);

        overlayView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        rootLayout.addView(overlayView);
    }


    public void layoutOff(){
        if (overlayView != null) {
            rootLayout.removeView(overlayView);
            overlayView = null;
        }
    }


    public void lockAllButtons(){
        for(int i = 0;i<fieldButtons.length;i++){
            for(int j = 0;j<fieldButtons[i].length;j++){
                fieldButtons[i][j].setClickable(false);
            }
        }

    }

    public void lockInputFields(){
        solutionA.setFocusable(false);
        solutionA.setFocusableInTouchMode(false);
        solutionA.setClickable(false);

        solutionB.setFocusable(false);
        solutionB.setFocusableInTouchMode(false);
        solutionB.setClickable(false);

        solutionC.setFocusable(false);
        solutionC.setFocusableInTouchMode(false);
        solutionC.setClickable(false);

        solutionD.setFocusable(false);
        solutionD.setFocusableInTouchMode(false);
        solutionD.setClickable(false);

        finalSolution.setFocusable(false);
        finalSolution.setFocusableInTouchMode(false);
        finalSolution.setClickable(false);


        AButton.setClickable(false);
        BButton.setClickable(false);
        CButton.setClickable(false);
        DButton.setClickable(false);
        finalSolution.setClickable(false);
    }

    public void unlockInputFields(){
        solutionA.setFocusable(true);
        solutionA.setFocusableInTouchMode(true);
        solutionA.setClickable(true);

        solutionB.setFocusable(true);
        solutionB.setFocusableInTouchMode(true);
        solutionB.setClickable(true);

        solutionC.setFocusable(true);
        solutionC.setFocusableInTouchMode(true);
        solutionC.setClickable(true);

        solutionD.setFocusable(true);
        solutionD.setFocusableInTouchMode(true);
        solutionD.setClickable(true);

        finalSolution.setFocusable(true);
        finalSolution.setFocusableInTouchMode(true);
        finalSolution.setClickable(true);


        AButton.setClickable(true);
        BButton.setClickable(true);
        CButton.setClickable(true);
        DButton.setClickable(true);
        finalSolution.setClickable(true);

        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(snapshot.child("Aguessed")==null){
                        solutionA.setFocusable(false);
                        solutionA.setFocusableInTouchMode(false);
                        solutionA.setClickable(false);
                    }
                    if(snapshot.child("Bguessed")==null){
                        solutionB.setFocusable(false);
                        solutionB.setFocusableInTouchMode(false);
                        solutionB.setClickable(false);
                    } if(snapshot.child("Vguessed")==null){
                        solutionC.setFocusable(false);
                        solutionC.setFocusableInTouchMode(false);
                        solutionC.setClickable(false);
                    } if(snapshot.child("Gguessed")==null){
                        solutionD.setFocusable(false);
                        solutionD.setFocusableInTouchMode(false);
                        solutionD.setClickable(false);
                    } if(snapshot.child("finalSolution")==null){
                        finalSolution.setFocusable(false);
                        finalSolution.setFocusableInTouchMode(false);
                        finalSolution.setClickable(false);
                    }



                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void endTurn(){
        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(playerValue.equals(snapshot.child("turn").getValue(String.class))&&playerValue.equals("player1")){
                        asocijacijeReference.child("turn").setValue("player2")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }else if(playerValue.equals(snapshot.child("turn").getValue(String.class))&&playerValue.equals("player2")){
                        asocijacijeReference.child("turn").setValue("player1")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void checkTurn(){
        asocijacijeReference.child("turn").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                   if(snapshot.getValue(String.class)!=null){
                       if(playerValue.equals(snapshot.getValue(String.class))){
                           layoutOff();
                           startTurn();
                       }else{
                           layoutOn();
                           lockAllButtons();
                           lockInputFields();
                       }
                   }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void startTurn(){
        unlockAllButtons();
        lockInputFields();
        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String Aopen = snapshot.child("Aopen").getValue(String.class);
                    String Bopen = snapshot.child("Bopen").getValue(String.class);
                    String Copen = snapshot.child("Vopen").getValue(String.class);
                    String Dopen = snapshot.child("Gopen").getValue(String.class);
                    if(Aopen!=null && Bopen!=null && Copen!=null && Dopen!=null){
                        if(Aopen.length() == 10 && Bopen.length() == 10 && Copen.length() == 10 && Dopen.length() == 10){
                            unlockInputFields();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void unlockAllButtons(){
        for(int i = 0;i<fieldButtons.length;i++){
            for(int j = 0;j<fieldButtons[i].length;j++){
                fieldButtons[i][j].setClickable(true);
            }
        }

        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    List<Integer> openFields = new ArrayList<>();

                    String input = snapshot.child("Aopen").getValue(String.class);

                    List<String> numberStrings = new ArrayList<String>();

                  if(input !=null){
                      numberStrings = Arrays.asList(input.split(",\\s*"));

                      for (String numberString : numberStrings) {
                          try {
                              int number = 0;
                              number= Integer.parseInt(numberString);
                              openFields.add(number);
                          } catch (NumberFormatException e) {
                          }
                      }

                      if(!openFields.isEmpty()){
                          for(int i = 0;i<openFields.size();i++){
                              fieldButtons[0][openFields.get(i)-1].setClickable(false);
                          }
                      }
                  }

                    openFields.clear();

                    input = snapshot.child("Bopen").getValue(String.class);

                    if(input!=null){
                        numberStrings = Arrays.asList(input.split(",\\s*"));

                        for (String numberString : numberStrings) {
                            try {
                                int number = 0;
                                number= Integer.parseInt(numberString);
                                openFields.add(number);
                            } catch (NumberFormatException e) {
                            }
                        }

                        if(!openFields.isEmpty()){
                            for(int i = 0;i<openFields.size();i++){
                                fieldButtons[1][openFields.get(i)-1].setClickable(false);
                            }
                        }

                    }

                    openFields.clear();

                    input = snapshot.child("Vopen").getValue(String.class);

                    if(input!=null){
                        numberStrings = Arrays.asList(input.split(",\\s*"));

                        for (String numberString : numberStrings) {
                            try {
                                int number = 0;
                                number= Integer.parseInt(numberString);
                                openFields.add(number);
                            } catch (NumberFormatException e) {
                            }
                        }

                        if(!openFields.isEmpty()){
                            for(int i = 0;i<openFields.size();i++){
                                fieldButtons[2][openFields.get(i)-1].setClickable(false);
                            }
                        }

                    }

                    openFields.clear();

                    input = snapshot.child("Gopen").getValue(String.class);

                    if(input!=null){
                        numberStrings = Arrays.asList(input.split(",\\s*"));

                        for (String numberString : numberStrings) {
                            try {
                                int number = 0;
                                number= Integer.parseInt(numberString);
                                openFields.add(number);
                            } catch (NumberFormatException e) {
                            }
                        }

                        if(!openFields.isEmpty()){
                            for(int i = 0;i<openFields.size();i++){
                                fieldButtons[3][openFields.get(i)-1].setClickable(false);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


    public static ArrayList<Integer> generateRandomArray(int min, int max, int count) {
        if (count > max - min + 1) {
            throw new IllegalArgumentException("Cannot generate more unique random numbers than available range.");
        }

        ArrayList<Integer> result = new ArrayList<>();
        ArrayList<Integer> availableNumbers = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            availableNumbers.add(i);
        }

        Random random = new Random();

        while (result.size() < count) {
            int randomIndex = random.nextInt(availableNumbers.size());
            int randomNumber = availableNumbers.get(randomIndex);
            availableNumbers.remove(randomIndex);
            result.add(randomNumber);
        }

        return result;
    }

    private void prepareGame(){
        if(isSecond&&getIntent().getIntExtra("GAME_NUMBER",-1)!=-1){
                    randomNumbers = new ArrayList<>();
                    randomNumbers.add(getIntent().getIntExtra("GAME_NUMBER",-1));
                    asocijacijeReference.child("status").setValue("gameprepared")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                    asocijacijeReference.child("turn").setValue("player2")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

        }else{
            randomNumbers = generateRandomArray(1, NUMBER_OF_QUESTIONS, 2);


            StringBuilder questionsData = new StringBuilder();

            for (int i = 0; i < randomNumbers.size(); i++) {
                questionsData.append(randomNumbers.get(i));

                if (i < randomNumbers.size() - 1) {
                    questionsData.append(", ");
                }
            }

            asocijacijeReference.child("questions").setValue(questionsData.toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            asocijacijeReference.child("player1points").setValue("0")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            asocijacijeReference.child("player2points").setValue("0")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            asocijacijeReference.child("status").setValue("gameprepared")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            asocijacijeReference.child("turn").setValue("player1")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }


    }





    public void determinePlayer(String player){
        playerValue = player;
    }

    private void startGameListener(){
        ValueEventListener statusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue(String.class);
                    if(status.equals("start")){
                        dialog.dismiss();
                        handler.removeCallbacksAndMessages(null);
                        startGame();
                        asocijacijeReference.child("status").removeEventListener(this);

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        listenerManager.addValueEventListener(asocijacijeReference.child("status"),statusListener);

    }


    private void preparedGameListener(){

        ValueEventListener statusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue(String.class);
                    if(status.equals("gameprepared")){
                        readData();
                        dialog.dismiss();
                        handler.removeCallbacksAndMessages(null);
                        asocijacijeReference.child("status").setValue("start")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }else{
                                            startGame();
                                        }
                                    }
                                });
                        asocijacijeReference.child("status").removeEventListener(this);

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        listenerManager.addValueEventListener(asocijacijeReference.child("status"),statusListener);
    }

    private void readData(){
        randomNumbers = new ArrayList<>();
        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    String input = snapshot.child("questions").getValue(String.class);

                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);

                            randomNumbers.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    public void updateData(){
        ValueEventListener Aopen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<Integer> array = new ArrayList<>();

                String input = snapshot.child("Aopen").getValue(String.class);

                if(input!=null){

                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);

                            array.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }
                }

                String isAGuessed = snapshot.child("Aguessed").getValue(String.class);


                if(!array.isEmpty()){
                    if(isAGuessed!=null){
                        if(isAGuessed.equals("player1")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[0][array.get(i)-1].setClickable(false);
                                fieldButtons[0][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                fieldButtons[0][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }else if(isAGuessed.equals("player2")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[0][array.get(i)-1].setClickable(false);
                                fieldButtons[0][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                fieldButtons[0][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }
                        for(int i = 0;i<4;i++){
                            showField(0, i);
                        }
                        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                solutionA.setText(snapshot.child("A").child("resenje").getValue(String.class));
                                solutionA.setFocusable(false);
                                solutionA.setFocusableInTouchMode(false);
                                solutionA.setClickable(false);
                                if(isAGuessed.equals("player1")){
                                    solutionA.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                    solutionA.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                                }else if(isAGuessed.equals("player2")){
                                    solutionA.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                    solutionA.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                   else{
                        for(int i = 0; i<array.size();i++){
                            fieldButtons[0][array.get(i)-1].setClickable(false);
                            fieldButtons[0][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.gameButtonColor)));
                            fieldButtons[0][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.black));
                        }
                    }

                    for(int i = 0;i<array.size();i++){
                            showField(0, array.get(i)-1);
                    }
                }




            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(asocijacijeReference,Aopen);

        ValueEventListener Bopen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<Integer> array = new ArrayList<>();

                String input = snapshot.child("Bopen").getValue(String.class);

                if(input!=null){
                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);

                            array.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }
                }

                String isBGuessed = snapshot.child("Bguessed").getValue(String.class);


                if(!array.isEmpty()){
                    if(isBGuessed!=null){
                        if(isBGuessed.equals("player1")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[1][array.get(i)-1].setClickable(false);
                                fieldButtons[1][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                fieldButtons[1][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }else if(isBGuessed.equals("player2")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[1][array.get(i)-1].setClickable(false);
                                fieldButtons[1][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                fieldButtons[1][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }
                        for(int i = 0;i<4;i++){
                            showField(1, i);
                        }
                        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                solutionB.setText(snapshot.child("B").child("resenje").getValue(String.class));
                                solutionB.setFocusable(false);
                                solutionB.setFocusableInTouchMode(false);
                                solutionB.setClickable(false);
                                if(isBGuessed.equals("player1")){
                                    solutionB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                    solutionB.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                                }else if(isBGuessed.equals("player2")){
                                    solutionB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                    solutionB.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                    else{
                        for(int i = 0; i<array.size();i++){
                            fieldButtons[1][array.get(i)-1].setClickable(false);
                            fieldButtons[1][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.gameButtonColor)));
                            fieldButtons[1][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.black));
                        }
                    }
                    for(int i = 0;i<array.size();i++){
                            showField(1, array.get(i)-1);
                    }

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(asocijacijeReference,Bopen);

        ValueEventListener Copen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<Integer> array = new ArrayList<>();

                String input = snapshot.child("Vopen").getValue(String.class);

                if(input!=null){
                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);

                            array.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }

                }

                String isCGuessed = snapshot.child("Vguessed").getValue(String.class);


                if(!array.isEmpty()){
                    if(isCGuessed!=null){
                        if(isCGuessed.equals("player1")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[2][array.get(i)-1].setClickable(false);
                                fieldButtons[2][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                fieldButtons[2][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }else if(isCGuessed.equals("player2")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[2][array.get(i)-1].setClickable(false);
                                fieldButtons[2][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                fieldButtons[2][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }
                        for(int i = 0;i<4;i++){
                            showField(2, i);
                        }
                        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                solutionC.setText(snapshot.child("V").child("resenje").getValue(String.class));
                                solutionC.setFocusable(false);
                                solutionC.setFocusableInTouchMode(false);
                                solutionC.setClickable(false);
                                if(isCGuessed.equals("player1")){
                                    solutionC.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                    solutionC.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                                }else if(isCGuessed.equals("player2")){
                                    solutionC.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                    solutionC.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                    else{
                        for(int i = 0; i<array.size();i++){
                            fieldButtons[2][array.get(i)-1].setClickable(false);
                            fieldButtons[2][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.gameButtonColor)));
                            fieldButtons[2][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.black));
                        }
                    }
                    for(int i = 0;i<array.size();i++){
                            showField(2, array.get(i)-1);

                    }

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(asocijacijeReference,Copen);

        ValueEventListener Dopen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<Integer> array = new ArrayList<>();

                String input = snapshot.child("Gopen").getValue(String.class);

                if(input!=null){
                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);

                            array.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }

                }

                String isDGuessed = snapshot.child("Gguessed").getValue(String.class);


                if(!array.isEmpty()){
                    if(isDGuessed!=null){
                        if(isDGuessed.equals("player1")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[3][array.get(i)-1].setClickable(false);
                                fieldButtons[3][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                fieldButtons[3][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }else if(isDGuessed.equals("player2")){
                            for(int i = 0; i<array.size();i++){
                                fieldButtons[3][array.get(i)-1].setClickable(false);
                                fieldButtons[3][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                fieldButtons[3][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }
                        for(int i = 0;i<4;i++){
                            showField(3, i);
                        }

                        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            solutionD.setText(snapshot.child("G").child("resenje").getValue(String.class));
                            solutionD.setFocusable(false);
                            solutionD.setFocusableInTouchMode(false);
                            solutionD.setClickable(false);
                            if(isDGuessed.equals("player1")){
                                solutionD.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                                solutionD.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }else if(isDGuessed.equals("player2")){
                                solutionD.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                                solutionD.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                    }
                    else{
                        for(int i = 0; i<array.size();i++){
                            fieldButtons[3][array.get(i)-1].setClickable(false);
                            fieldButtons[3][array.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.gameButtonColor)));
                            fieldButtons[3][array.get(i)-1].setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.black));
                        }
                    }

                    for(int i = 0;i<array.size();i++){
                            showField(3, array.get(i)-1);

                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(asocijacijeReference,Dopen);

        ValueEventListener finalListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){

                    String playerWinner =snapshot.child("finalguessed").getValue(String.class);
                    if(playerWinner!=null){
                        if(playerWinner.equals("player1")){
                            finalSolution.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.blue)));
                            finalSolution.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                        }else{
                            finalSolution.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(AsocijacijeActivity.this, R.color.red)));
                            finalSolution.setTextColor(ContextCompat.getColor(AsocijacijeActivity.this, R.color.white));
                        }

                        finalSolution.setFocusable(false);
                        finalSolution.setFocusableInTouchMode(false);
                        finalSolution.setClickable(false);

                        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                finalSolution.setText(snapshot.child("KonacnoResenje").getValue(String.class));

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(asocijacijeReference,finalListener);

        ValueEventListener nextGameListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(snapshot.getValue(String.class)!=null){


                       nextGame();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
        listenerManager.addValueEventListener(asocijacijeReference.child("finalguessed"),nextGameListener);


    }


    public void startGame(){
        activeQuestion = randomNumbers.get(0);
        checkTurn();
        countDownTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                timer.setText(Long.toString(millisUntilFinished/1000));
            }
            public void onFinish() {
                nextGame();
            }
        }.start();


    }

    public void nextGame(){

        countDownTimer.cancel();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if(randomNumbers.size()==1){
                    Toast.makeText(AsocijacijeActivity.this, "Крај игре Асоцијације.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                    intent.putExtra("GAME_ID", gameId);
                    intent.putExtra("NEXT_GAME", "EndGame");
                    leaveIntent  = false;
                    startActivity(intent);
                    finish();
                    return;
                }

                randomNumbers.remove(0);



                listenerManager.removeAllListeners();

                asocijacijeReference.child("Aguessed").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Bguessed").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Vguessed").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Gguessed").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Aopen").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Bopen").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Vopen").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("Gopen").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                asocijacijeReference.child("finalguessed").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });

                activeQuestion = randomNumbers.get(0);
                asocijacijeReference.child("questions").setValue(Integer.toString(activeQuestion))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                Intent intent = new Intent(getApplicationContext(), AsocijacijeActivity.class);
                intent.putExtra("GAME_ID", gameId);
                intent.putExtra("SECOND_GAME",true);
                intent.putExtra("GAME_NUMBER", activeQuestion);
                leaveIntent = false;
                startActivity(intent);
                finish();


            }
        }.start();



    }

    private void initializeUI() {
        fieldButtons[0][0] = findViewById(R.id.buttonA1);
        fieldButtons[0][1] = findViewById(R.id.buttonA2);
        fieldButtons[0][2] = findViewById(R.id.buttonA3);
        fieldButtons[0][3] = findViewById(R.id.buttonA4);

        fieldButtons[1][0] = findViewById(R.id.buttonB1);
        fieldButtons[1][1] = findViewById(R.id.buttonB2);
        fieldButtons[1][2] = findViewById(R.id.buttonB3);
        fieldButtons[1][3] = findViewById(R.id.buttonB4);

        fieldButtons[2][0] = findViewById(R.id.buttonV1);
        fieldButtons[2][1] = findViewById(R.id.buttonV2);
        fieldButtons[2][2] = findViewById(R.id.buttonV3);
        fieldButtons[2][3] = findViewById(R.id.buttonV4);

        fieldButtons[3][0] = findViewById(R.id.buttonG1);
        fieldButtons[3][1] = findViewById(R.id.buttonG2);
        fieldButtons[3][2] = findViewById(R.id.buttonG3);
        fieldButtons[3][3] = findViewById(R.id.buttonG4);

        finalSolution = findViewById(R.id.finalSolution);
        solutionA = findViewById(R.id.columnA);
        solutionB = findViewById(R.id.columnB);
        solutionC = findViewById(R.id.columnV);
        solutionD = findViewById(R.id.columnG);

        AButton = findViewById(R.id.sendButtonA);
        BButton = findViewById(R.id.sendButtonB);
        CButton = findViewById(R.id.sendButtonV);
        DButton = findViewById(R.id.sendButtonG);
        finalButton = findViewById(R.id.sendButtonFinal);

    }


    private void setButtonListeners() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                final int row = i;
                final int col = j;

                fieldButtons[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openField(row, col);
                    }
                });
            }
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void showField(int row, int col){
        Button clickedButton = fieldButtons[row][col];
        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

        String rowString;

        if(row==0){
            rowString = "A";
        }else if(row==1){
            rowString = "B";
        }else if(row==2){
            rowString = "V";
        }else{
            rowString = "G";
        }

        questionReference = questionReference.child(rowString).child(Integer.toString(col+1));

        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    clickedButton.setText(snapshot.getValue(String.class));
                    clickedButton.setEnabled(false);

                    openedFieldsCount++;

                    if (openedFieldsCount > 0) {
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    private void setTextFieldsListeners(){
        AButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                hideKeyboard();

                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String enteredText = solutionA.getText().toString();

                        if(enteredText.equals(snapshot.child("A").child("resenje").getValue(String.class))){


                            asocijacijeReference.child("Aguessed").setValue(playerValue)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });


                            asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                        points+=2;

                                        String aGuessed = snapshot.child("Aguessed").getValue(String.class);
                                        if(aGuessed==null){
                                            points+=4;
                                        }else{
                                            int count = 0;
                                            if(aGuessed!=null){
                                                for(int i = 0;i<aGuessed.length();i++){
                                                    if (aGuessed.charAt(i) == ',') {
                                                        count++;
                                                    }
                                                }
                                            }

                                            points+=4-1-count;
                                        }

                                        asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                            asocijacijeReference.child("Aopen").setValue("1, 2, 3, 4")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });



                        }else{
                            solutionA.setText("");
                            lockInputFields();
                            lockAllButtons();
                            endTurn();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        BButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                hideKeyboard();


                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String enteredText = solutionB.getText().toString();

                        if(enteredText.equals(snapshot.child("B").child("resenje").getValue(String.class))){



                            asocijacijeReference.child("Bguessed").setValue(playerValue)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                            asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                        points+=2;

                                        String bGuessed = snapshot.child("Bguessed").getValue(String.class);
                                        if(bGuessed==null){
                                            points+=4;
                                        }else{
                                            int count = 0;
                                            if(bGuessed!=null){
                                                for(int i = 0;i<bGuessed.length();i++){
                                                    if (bGuessed.charAt(i) == ',') {
                                                        count++;
                                                    }
                                                }
                                            }

                                            points+=4-1-count;
                                        }

                                        asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });


                            asocijacijeReference.child("Bopen").setValue("1, 2, 3, 4")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }else{
                            solutionB.setText("");
                            lockInputFields();
                            lockAllButtons();
                            endTurn();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        CButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                hideKeyboard();


                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String enteredText = solutionC.getText().toString();

                        if(enteredText.equals(snapshot.child("V").child("resenje").getValue(String.class))){



                            asocijacijeReference.child("Vguessed").setValue(playerValue)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                        points+=2;

                                        String cGuessed = snapshot.child("Vguessed").getValue(String.class);
                                        if(cGuessed==null){
                                            points+=4;
                                        }else{
                                            int count = 0;
                                            if(cGuessed!=null){
                                                for(int i = 0;i<cGuessed.length();i++){
                                                    if (cGuessed.charAt(i) == ',') {
                                                        count++;
                                                    }
                                                }
                                            }

                                            points+=4-1-count;
                                        }

                                        asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });


                            asocijacijeReference.child("Vopen").setValue("1, 2, 3, 4")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }else{
                            solutionC.setText("");
                            lockInputFields();
                            lockAllButtons();
                            endTurn();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        DButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                hideKeyboard();

                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String enteredText = solutionD.getText().toString();

                        if(enteredText.equals(snapshot.child("G").child("resenje").getValue(String.class))){



                            asocijacijeReference.child("Gguessed").setValue(playerValue)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                            asocijacijeReference.child("Gopen").setValue("1, 2, 3, 4")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                        points+=2;

                                        String dGuessed = snapshot.child("Gguessed").getValue(String.class);
                                        if(dGuessed==null){
                                            points+=4;
                                        }else{
                                            int count = 0;
                                            if(dGuessed!=null){
                                                for(int i = 0;i<dGuessed.length();i++){
                                                    if (dGuessed.charAt(i) == ',') {
                                                        count++;
                                                    }
                                                }
                                            }

                                            points+=4-1-count;
                                        }

                                        asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                        }else{
                            solutionD.setText("");
                            lockInputFields();
                            lockAllButtons();
                            endTurn();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        finalButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                hideKeyboard();


                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String enteredText = finalSolution.getText().toString();

                        if(enteredText.equals(snapshot.child("KonacnoResenje").getValue(String.class))){



                            asocijacijeReference.child("finalguessed").setValue(playerValue)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                        points+=7;
                                        if(snapshot.child("Aguessed").getValue(String.class)==null){
                                            points+=6;
                                        }
                                        if(snapshot.child("Bguessed").getValue(String.class)==null){
                                            points+=6;
                                        } if(snapshot.child("Vguessed").getValue(String.class)==null){
                                            points+=6;
                                        } if(snapshot.child("Gguessed").getValue(String.class)==null){
                                            points+=6;
                                        }

                                        asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });


                            asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.child("Aguessed").getValue(String.class) == null){
                                        asocijacijeReference.child("Aguessed").setValue(playerValue)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                                    points+=2;

                                                    String aGuessed = snapshot.child("Aguessed").getValue(String.class);
                                                    if(aGuessed==null){
                                                        points+=4;
                                                    }else{
                                                        int count = 0;
                                                        if(aGuessed!=null){
                                                            for(int i = 0;i<aGuessed.length();i++){
                                                                if (aGuessed.charAt(i) == ',') {
                                                                    count++;
                                                                }
                                                            }
                                                        }

                                                        points+=4-1-count;
                                                    }

                                                    asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (!task.isSuccessful()) {
                                                                        Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        asocijacijeReference.child("Aopen").setValue("1, 2, 3, 4")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                    if(snapshot.child("Bguessed").getValue(String.class) == null){
                                        asocijacijeReference.child("Bguessed").setValue(playerValue)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                                    points+=2;

                                                    String bGuessed = snapshot.child("Bguessed").getValue(String.class);
                                                    if(bGuessed==null){
                                                        points+=4;
                                                    }else{
                                                        int count = 0;
                                                        if(bGuessed!=null){
                                                            for(int i = 0;i<bGuessed.length();i++){
                                                                if (bGuessed.charAt(i) == ',') {
                                                                    count++;
                                                                }
                                                            }
                                                        }

                                                        points+=4-1-count;
                                                    }

                                                    asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (!task.isSuccessful()) {
                                                                        Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        asocijacijeReference.child("Bopen").setValue("1, 2, 3, 4")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                    if(snapshot.child("Vguessed").getValue(String.class) == null){
                                        asocijacijeReference.child("Vguessed").setValue(playerValue)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                                    points+=2;

                                                    String cGuessed = snapshot.child("Vguessed").getValue(String.class);
                                                    if(cGuessed==null){
                                                        points+=4;
                                                    }else{
                                                        int count = 0;
                                                        if(cGuessed!=null){
                                                            for(int i = 0;i<cGuessed.length();i++){
                                                                if (cGuessed.charAt(i) == ',') {
                                                                    count++;
                                                                }
                                                            }
                                                        }

                                                        points+=4-1-count;
                                                    }

                                                    asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (!task.isSuccessful()) {
                                                                        Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        asocijacijeReference.child("Vopen").setValue("1, 2, 3, 4")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                    if(snapshot.child("Gguessed").getValue(String.class) == null){
                                        asocijacijeReference.child("Gguessed").setValue(playerValue)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                        asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));

                                                    points+=2;

                                                    String dGuessed = snapshot.child("Gguessed").getValue(String.class);
                                                    if(dGuessed==null){
                                                        points+=4;
                                                    }else{
                                                        int count = 0;
                                                        if(dGuessed!=null){
                                                            for(int i = 0;i<dGuessed.length();i++){
                                                                if (dGuessed.charAt(i) == ',') {
                                                                    count++;
                                                                }
                                                            }
                                                        }

                                                        points+=4-1-count;
                                                    }

                                                    asocijacijeReference.child(playerValue+"points").setValue(Integer.toString(points))
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (!task.isSuccessful()) {
                                                                        Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        asocijacijeReference.child("Gopen").setValue("1, 2, 3, 4")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }






                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                        }else{
                            finalSolution.setText("");
                            lockInputFields();
                            lockAllButtons();
                            endTurn();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
    }



    private void openField(int row, int col) {
        Button clickedButton = fieldButtons[row][col];


        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("asocijacije").child(Integer.toString(activeQuestion));

        String rowString;



        if(row==0){
            rowString = "A";
        }else if(row==1){
            rowString = "B";
        }else if(row==2){
            rowString = "V";
        }else{
            rowString = "G";
        }

        questionReference = questionReference.child(rowString).child(Integer.toString(col+1));




        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if (!clickedButton.isEnabled()) {
                        return;
                    }
                    clickedButton.setText(snapshot.getValue(String.class));
                    clickedButton.setEnabled(false);

                    asocijacijeReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){

                                String opened = snapshot.child(rowString+"open").getValue(String.class);

                                int count = 0;
                                if(opened!=null){
                                    for(int i = 0;i<opened.length();i++){
                                        if (opened.charAt(i) == ',') {
                                            count++;
                                        }
                                    }
                                }


                                if(count==0 && opened==null){
                                    opened = Integer.toString(col + 1);
                                } else {
                                    opened += ", "+Integer.toString(col + 1);
                                }

                                asocijacijeReference.child(rowString+"open").setValue(opened)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                lockAllButtons();
                                                unlockInputFields();
                                                if (!task.isSuccessful()) {
                                                    Toast.makeText(AsocijacijeActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });

                                openedFieldsCount++;


                                if (openedFieldsCount > 0) {
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }



    @Override
    protected void onDestroy() {

        if(gameId!=null && leaveIntent == true){
            final DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        String userId = user.getUid();

                        if(snapshot.child("player1").getValue().equals(userId)){
                            gameRef.child( "player1left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player2left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }else if(snapshot.child("player2").getValue().equals(userId)){
                            gameRef.child( "player2left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player1left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                }
            });

        }




        super.onDestroy();
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
        handler.removeCallbacksAndMessages(null);
        listenerManager.removeAllListeners();
    }


}
