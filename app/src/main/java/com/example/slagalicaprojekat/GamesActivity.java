package com.example.slagalicaprojekat;

import static android.app.PendingIntent.getActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

import com.example.slagalicaprojekat.FirebaseListenerManager;
import com.google.firebase.database.annotations.Nullable;

public class GamesActivity extends AppCompatActivity {

    private Button leaveButton;
    private Button koznaznaButton, spojniceButton, asocijacijeButton, skockoButton, korakpokorakButton, mojbrojButton;
    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();
    FirebaseAuth auth;
    FirebaseUser user;
    String guestId;
    String gameId;
    Boolean isInvited;
    String foundedUserId = null;
    Dialog dialog;
    private Boolean leaveIntent = true, leaveGameIntent = true;
    Handler handler = new Handler();
    public String inGameId = null;
    public String userId;
    public String playerValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();

        isInvited = getIntent().getBooleanExtra("IS_INVITED", false);
        gameId = getIntent().getStringExtra("GAME_ID");
        dialog = ProgressDialog.show(GamesActivity.this, "",
                "Учитавање игре...", true);
        dialog.show();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                abortGame("Прошло је време за учитавање игре.");
            }
        };
        handler.postDelayed(runnable, 10000);


        leaveButton = findViewById(R.id.leaveButton);

        koznaznaButton = findViewById(R.id.koznaznaButton);
        spojniceButton = findViewById(R.id.spojniceButton);
        asocijacijeButton = findViewById(R.id.asocijacijeButton);
        korakpokorakButton = findViewById(R.id.korakpokorakButton);
        skockoButton = findViewById(R.id.skockoButton);
        mojbrojButton = findViewById(R.id.mojbrojButton);

        leaveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(user!=null && !user.isAnonymous()){
                    Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    leaveIntent = false;
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    leaveIntent = false;
                    startActivity(intent);
                    finish();
                }
            }
        });

        koznaznaButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), KoZnaZnaActivity.class);
                if(gameId!=null){
                    intent.putExtra("GAME_ID", gameId);
                }else if(inGameId!=null){
                    intent.putExtra("GAME_ID",inGameId);
                }
                leaveIntent = false;
                leaveGameIntent = false;
                startActivity(intent);
                finish();
            }
        });

        spojniceButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), SpojniceActivity.class);
                if(gameId!=null){
                    intent.putExtra("GAME_ID", gameId);
                }else if(inGameId!=null){
                    intent.putExtra("GAME_ID",inGameId);
                }
                leaveIntent = false;
                leaveGameIntent = false;
                startActivity(intent);
                finish();
            }
        });

        asocijacijeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), AsocijacijeActivity.class);
                if(gameId!=null){
                    intent.putExtra("GAME_ID", gameId);
                }else if(inGameId!=null){
                    intent.putExtra("GAME_ID",inGameId);
                }
                leaveIntent = false;
                leaveGameIntent = false;
                startActivity(intent);
                finish();
            }
        });

        if(gameId!=null && !isInvited){
            DatabaseReference gameReference = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
            gameReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        if(snapshot.child("player1").getValue().equals(userId)){
                            determinePlayer("player1");
                        }else if(snapshot.child("player2").getValue().equals(userId)){
                            determinePlayer("player2");
                        }

                        if(gameReference.child(playerValue+"Location")!=null) {
                            dialog.dismiss();
                            handler.removeCallbacksAndMessages(null);
                            continueGame();
                        }else{
                            initializeGame(gameId,isInvited);
                        }

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        else{
            initializeGame(gameId,isInvited);
        }



    }

    public void initializeGame(String gameId, Boolean playerIsInvited){
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");

        if(!user.isAnonymous()) {
            activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                }
            });
        }else if(user.isAnonymous()){
            activeUsers.child(userId).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                }
            });
        }
        if(playerIsInvited){
            dialog.dismiss();
            handler.removeCallbacksAndMessages(null);
            DatabaseReference gameInvRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference currentGame = gameInvRef.child("games").child(gameId);
            currentGame.child("status").setValue("accepted") .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                }
            });
            startGame(gameId);
        }else{

            Boolean isFriendRequested = getIntent().getBooleanExtra("IS_FRIEND_REQUESTED",false);
            if(isFriendRequested == true){
                foundedUserId = getIntent().getStringExtra("FRIEND_ID");
                DatabaseReference foundedActiveUserReference = activeUsers.child(foundedUserId);
                DatabaseReference activeUsersRef = FirebaseDatabase.getInstance().getReference("users").child("active");
                activeUsersRef.child(foundedUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            if (snapshot.getValue(Boolean.class) != true) {
                                dialog.dismiss();
                                abortGame("Пријатељ тренутно није активан!");
                            }
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                ValueEventListener foundedActiveUserListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                };
                listenerManager.addValueEventListener(foundedActiveUserReference,foundedActiveUserListener);
                DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

                String gId = gamesReference.push().getKey();
                inGameId= gId;

                Map<String, Object> gameData = new HashMap<>();
                gameData.put("player1", userId);
                gameData.put("player2", foundedUserId);
                gameData.put("status", "pending");

                guestId = foundedUserId;

                gamesReference.child(inGameId).setValue(gameData)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                            }
                        });

                DatabaseReference statusRef = usersRef.child("games").child(inGameId).child("status");

                ValueEventListener statusListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.exists()) {
                            String status = dataSnapshot.getValue(String.class);
                            if (status != null) {
                                if (status.equals("accepted")) {
                                    startGame(inGameId);
                                    dialog.dismiss();
                                    handler.removeCallbacksAndMessages(null);
                                    statusRef.removeEventListener(this);

                                } else if (status.equals("declined")) {
                                    gamesReference.child(inGameId).removeValue()
                                            .addOnSuccessListener(aVoid -> {
                                            })
                                            .addOnFailureListener(e -> {
                                            });
                                    statusRef.removeEventListener(this);
                                    dialog.dismiss();
                                    abortGame("Играч који је пронађен, одбио је партију. Пробајте поново.");
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };

                listenerManager.addValueEventListener(statusRef,statusListener);
            }else{
                activeUsers.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Boolean available = false;
                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            foundedUserId = userSnapshot.getKey();

                            if(available){
                                break;
                            }

                            if(foundedUserId.equals(userId)){
                                continue;
                            }

                            if(foundedUserId==null)
                                abortGame("Нема слободног играча!");

                            if(foundedUserId!=null && userSnapshot.getValue(Boolean.class))
                            {
                                available = true;
                                DatabaseReference foundedActiveUserReference = activeUsers.child(foundedUserId);
                                ValueEventListener foundedActiveUserListener = new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                };
                                listenerManager.addValueEventListener(foundedActiveUserReference,foundedActiveUserListener);

                                DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");
                                String gId = gamesReference.push().getKey();
                                inGameId= gId;

                                Map<String, Object> gameData = new HashMap<>();
                                gameData.put("player1", userId);
                                gameData.put("player2", foundedUserId);
                                gameData.put("status", "pending");

                                guestId = foundedUserId;

                                gamesReference.child(inGameId).setValue(gameData)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                            }
                                        });

                                DatabaseReference statusRef = usersRef.child("games").child(inGameId).child("status");

                                ValueEventListener statusListener = new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        if (dataSnapshot.exists()) {
                                            String status = dataSnapshot.getValue(String.class);
                                            if (status != null) {
                                                if (status.equals("accepted")) {
                                                    startGame(inGameId);
                                                    dialog.dismiss();
                                                    handler.removeCallbacksAndMessages(null);
                                                    statusRef.removeEventListener(this);

                                                } else if (status.equals("declined")) {
                                                    gamesReference.child(inGameId).removeValue()
                                                            .addOnSuccessListener(aVoid -> {
                                                            })
                                                            .addOnFailureListener(e -> {
                                                            });
                                                    statusRef.removeEventListener(this);
                                                    dialog.dismiss();
                                                    abortGame("Играч који је пронађен, одбио је партију. Пробајте поново.");
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                };

                                listenerManager.addValueEventListener(statusRef,statusListener);

                            } else {
                                continue;
                            }




                        }
                        if(!available){
                            abortGame("Нема слободног играча");

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }

        }

    }

    public void abortGame(String abortReason){
        if(abortReason!=null || abortReason!=""){
            Toast.makeText(GamesActivity.this, abortReason, Toast.LENGTH_SHORT).show();
        }
        handler.removeCallbacksAndMessages(null);
        if(inGameId!=null){
            DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");
            gamesReference.child(inGameId).removeValue()
                    .addOnSuccessListener(aVoid -> {
                    })
                    .addOnFailureListener(e -> {
                    });
        }

        if(user.isAnonymous()){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }


    }


    public void startGame(String gameId){
        Toast.makeText(GamesActivity.this, "Игра је учитана", Toast.LENGTH_SHORT).show();
        DatabaseReference gameReference = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String player1Id = dataSnapshot.child("player1").getValue(String.class);
                    String player2Id = dataSnapshot.child("player2").getValue(String.class);

                    if (userId.equals(player1Id)) {
                        gameReference.child("player1Location").setValue("main").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(!user.isAnonymous()) {
                                    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users").child(userId);
                                    userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            if (snapshot.exists()) {
                                                Long tokens = snapshot.child("tokens").getValue(Long.class);
                                                if (tokens > 0)
                                                    tokens--;
                                                userRef.child("tokens").setValue(tokens);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });
                                }
                            }
                        });
                        gameReference.removeEventListener(this);
                    } else if (userId.equals(player2Id)) {
                        gameReference.child("player2Location").setValue("main").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(!user.isAnonymous()) {
                                    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users").child(userId);
                                    userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            if (snapshot.exists()) {
                                                Long tokens = snapshot.child("tokens").getValue(Long.class);
                                                if (tokens > 0)
                                                    tokens--;
                                                userRef.child("tokens").setValue(tokens);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });
                                }
                            }
                        });
                        gameReference.removeEventListener(this);
                    } else {

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        listenerManager.addValueEventListener(gameReference,valueEventListener);

        String currentGame = "KoZnaZna";
        buttonController(currentGame);
    }

    public void updatePoints(){
        TextView koznaznap, koznaznapo, spojnicep, spojnicepo, asocijacijep, asocijacijepo,total, totalo;
        koznaznap = findViewById(R.id.koznaznaPoints);
        koznaznapo = findViewById(R.id.koznaznaPointsOpponent);
        spojnicep = findViewById(R.id.spojnicePoints);
        spojnicepo = findViewById(R.id.spojnicePointsOpponent);
        asocijacijep = findViewById(R.id.asocijacijePoints);
        asocijacijepo = findViewById(R.id.asocijacijePointsOpponent);
        total = findViewById(R.id.total);
        totalo = findViewById(R.id.totalOpponent);

        DatabaseReference gameReference = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);

        gameReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(playerValue.equals("player1")){
                        int totalpInt, totalpoInt;
                        totalpInt = totalpoInt = 0;
                        String koznaznapString = snapshot.child("koZnaZnaGame").child("player1points").getValue(String.class);
                        if(koznaznapString!=null){
                            koznaznap.setText(koznaznapString);
                            totalpInt+=Integer.parseInt(koznaznapString);
                        }
                        String spojnicepString = snapshot.child("spojniceGame").child("player1points").getValue(String.class);
                        if(spojnicepString!=null){
                            spojnicep.setText(spojnicepString);
                            totalpInt+=Integer.parseInt(spojnicepString);
                        }
                        String asocijacijepString = snapshot.child("asocijacijeGame").child("player1points").getValue(String.class);
                        if(asocijacijepString!=null){
                            asocijacijep.setText(asocijacijepString);
                            totalpInt+=Integer.parseInt(asocijacijepString);
                        }
                        String koznaznapoString = snapshot.child("koZnaZnaGame").child("player2points").getValue(String.class);
                        if(koznaznapoString!=null){
                            koznaznapo.setText(koznaznapoString);
                            totalpoInt+=Integer.parseInt(koznaznapoString);
                        }
                        String spojnicepoString = snapshot.child("spojniceGame").child("player2points").getValue(String.class);
                        if(spojnicepoString!=null){
                            spojnicepo.setText(spojnicepoString);
                            totalpoInt+=Integer.parseInt(spojnicepoString);
                        }
                        String asocijacijepoString = snapshot.child("asocijacijeGame").child("player2points").getValue(String.class);
                        if(asocijacijepoString!=null){
                            asocijacijepo.setText(asocijacijepoString);
                            totalpoInt+=Integer.parseInt(asocijacijepoString);

                        }
                        total.setText(Integer.toString(totalpInt));
                        totalo.setText(Integer.toString(totalpoInt));

                    }else{
                            int totalpInt, totalpoInt;
                            totalpInt = totalpoInt = 0;
                            String koznaznapString = snapshot.child("koZnaZnaGame").child("player2points").getValue(String.class);
                            if(koznaznapString!=null){
                                koznaznap.setText(koznaznapString);
                                totalpInt+=Integer.parseInt(koznaznapString);
                            }
                            String spojnicepString = snapshot.child("spojniceGame").child("player2points").getValue(String.class);
                            if(spojnicepString!=null){
                                spojnicep.setText(spojnicepString);
                                totalpInt+=Integer.parseInt(spojnicepString);
                            }
                            String asocijacijepString = snapshot.child("asocijacijeGame").child("player2points").getValue(String.class);
                            if(asocijacijepString!=null){
                                asocijacijep.setText(asocijacijepString);
                                totalpInt+=Integer.parseInt(asocijacijepString);
                            }
                            String koznaznapoString = snapshot.child("koZnaZnaGame").child("player1points").getValue(String.class);
                            if(koznaznapoString!=null){
                                koznaznapo.setText(koznaznapoString);
                                totalpoInt+=Integer.parseInt(koznaznapoString);
                            }
                            String spojnicepoString = snapshot.child("spojniceGame").child("player1points").getValue(String.class);
                            if(spojnicepoString!=null){
                                spojnicepo.setText(spojnicepoString);
                                totalpoInt+=Integer.parseInt(spojnicepoString);
                            }
                            String asocijacijepoString = snapshot.child("asocijacijeGame").child("player1points").getValue(String.class);
                            if(asocijacijepoString!=null){
                                asocijacijepo.setText(asocijacijepoString);
                                totalpoInt+=Integer.parseInt(asocijacijepoString);

                            }
                            total.setText(Integer.toString(totalpInt));
                            totalo.setText(Integer.toString(totalpoInt));

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void continueGame(){
        updatePoints();
        String nextGame = getIntent().getStringExtra("NEXT_GAME");
        buttonController(nextGame);
        if(nextGame.equals("EndGame")){
            leaveButton.setText("Заврши игру");
        }
        DatabaseReference gameReference = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
        gameReference.child(playerValue+"Location").setValue("main")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(GamesActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    public void buttonController(String nextGame){
        if(nextGame.equals("KoZnaZna")){
            koznaznaButton.setClickable(true);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.black));

            spojniceButton.setClickable(false);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            asocijacijeButton.setClickable(false);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            skockoButton.setClickable(false);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            korakpokorakButton.setClickable(false);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            mojbrojButton.setClickable(false);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        } else if (nextGame.equals("Spojnice")){
            spojniceButton.setClickable(true);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.black));

            koznaznaButton.setClickable(false);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            asocijacijeButton.setClickable(false);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            skockoButton.setClickable(false);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            korakpokorakButton.setClickable(false);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            mojbrojButton.setClickable(false);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        }else if(nextGame.equals("Asocijacije")){
            asocijacijeButton.setClickable(true);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.black));

            spojniceButton.setClickable(false);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            koznaznaButton.setClickable(false);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            skockoButton.setClickable(false);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            korakpokorakButton.setClickable(false);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            mojbrojButton.setClickable(false);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        }else if(nextGame.equals("Skocko")){
            skockoButton.setClickable(true);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.black));

            spojniceButton.setClickable(false);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            koznaznaButton.setClickable(false);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            asocijacijeButton.setClickable(false);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            korakpokorakButton.setClickable(false);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            mojbrojButton.setClickable(false);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
        else if(nextGame.equals("KorakPoKorak")){
            korakpokorakButton.setClickable(true);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.black));

            spojniceButton.setClickable(false);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            koznaznaButton.setClickable(false);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            skockoButton.setClickable(false);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            asocijacijeButton.setClickable(false);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            mojbrojButton.setClickable(false);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
        else if(nextGame.equals("MojBroj")){
            mojbrojButton.setClickable(true);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.black));

            spojniceButton.setClickable(false);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            koznaznaButton.setClickable(false);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            skockoButton.setClickable(false);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            korakpokorakButton.setClickable(false);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            asocijacijeButton.setClickable(false);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        }else{
            mojbrojButton.setClickable(false);
            mojbrojButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            mojbrojButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            spojniceButton.setClickable(false);
            spojniceButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            spojniceButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            koznaznaButton.setClickable(false);
            koznaznaButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            koznaznaButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            skockoButton.setClickable(false);
            skockoButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            skockoButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            korakpokorakButton.setClickable(false);
            korakpokorakButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            korakpokorakButton.setTextColor(ContextCompat.getColor(this, R.color.white));

            asocijacijeButton.setClickable(false);
            asocijacijeButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black)));
            asocijacijeButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    public void determinePlayer(String player){
        playerValue = player;
    }


    @Override
    public void onBackPressed() {
        if(user!=null && !user.isAnonymous()){
            Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }
        super.onBackPressed();
    }



    @Override
    protected void onDestroy() {

        if(gameId!=null && leaveGameIntent == true){
            final DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        String userId = user.getUid();
                        if(snapshot.child("player1").getValue().equals(userId)){
                            gameRef.child( "player1left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player2left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }else if(snapshot.child("player2").getValue().equals(userId)){
                            gameRef.child( "player2left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player1left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }else if(inGameId!= null && leaveGameIntent == true){
            final DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users").child("games").child(inGameId);
            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        String userId = user.getUid();

                        if(snapshot.child("player1").getValue().equals(userId)){
                            gameRef.child( "player1left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player2left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }else if(snapshot.child("player2").getValue().equals(userId)){
                            gameRef.child( "player2left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player1left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }

        if(leaveIntent == true){

            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            if(!user.isAnonymous()) {
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }else if(user.isAnonymous()){
                activeUsers.child(userId).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }
        }

        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
        listenerManager.removeAllListeners();
    }


}