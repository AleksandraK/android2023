package com.example.slagalicaprojekat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrationActivity extends AppCompatActivity {

    private EditText emailEditText, usernameEditText, passwordEditText, confirmPasswordEditText;
    private Button registerButton;
    private Button goOnLogInPage;

    private ImageButton backButton;

    private FirebaseAuth mAuth;

    FirebaseUser user;


    private Boolean leaveIntent = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mAuth = FirebaseAuth.getInstance();
        emailEditText = findViewById(R.id.email);
        usernameEditText = findViewById(R.id.usernameEditText);
        passwordEditText = findViewById(R.id.password);
        confirmPasswordEditText = findViewById(R.id.confirmPasswordEditText);
        registerButton = findViewById(R.id.loginButton);
        backButton = (ImageButton)findViewById(R.id.backButton);
        goOnLogInPage = findViewById(R.id.goOnLogInPage);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEditText.getText().toString();
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                String confirmPassword = confirmPasswordEditText.getText().toString();

                if (isValidInput(email, username, password, confirmPassword)) {

                    FirebaseUser currentUser = mAuth.getCurrentUser();
                    if(currentUser!=null && currentUser.isAnonymous()){
                            currentUser.delete()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {

                                            } else {
                                            }
                                        }
                                    });
                    }


                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {

                                        String userId = mAuth.getUid();
                                        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                                        DatabaseReference currentUserRef = usersRef.child(userId);
                                        String initialUsername = username;
                                        currentUserRef.child("username").setValue(initialUsername)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RegistrationActivity.this,R.string.errorWithSettingUsername , Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            currentUserRef.child("tokens").setValue("5")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RegistrationActivity.this,R.string.errorWithSettingData , Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                        currentUserRef.child("stars").setValue("0")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RegistrationActivity.this,R.string.errorWithSettingData , Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                        Toast.makeText(RegistrationActivity.this, R.string.successfulRegistrationMsg, Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(RegistrationActivity.this, RegistratedUserMain.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        leaveIntent = false;
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(RegistrationActivity.this, R.string.unsuccessfulRegistrationMsg,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                } else {
                    Toast.makeText(RegistrationActivity.this, R.string.fieldsErrorMsg, Toast.LENGTH_SHORT).show();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });
        goOnLogInPage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

    }
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null && !currentUser.isAnonymous()){
            Toast.makeText(RegistrationActivity.this, R.string.alreadyLoggedInMsg, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(RegistrationActivity.this, RegistratedUserMain.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            leaveIntent = false;
            startActivity(intent);
            finish();
        }
    }

    private boolean isValidInput(String email, String username, String password, String confirmPassword) {
        return !email.isEmpty() && !username.isEmpty() && !password.isEmpty() && password.equals(confirmPassword);
    }



    @Override
    public void onBackPressed() {
        leaveIntent = false;
        Intent intent  = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    protected void onDestroy() {

        if(leaveIntent == true){
            FirebaseAuth auth;
            FirebaseUser user;
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            if(user!=null){
                String userId = user.getUid();
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                DatabaseReference activeUsers = usersRef.child("active");
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }

        }
        super.onDestroy();
    }

}
