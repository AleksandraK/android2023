package com.example.slagalicaprojekat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventListener;
import java.util.List;
import java.util.Random;

import com.example.slagalicaprojekat.FirebaseListenerManager;


public class KoZnaZnaActivity extends AppCompatActivity {

    Handler handler = new Handler();
    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();

    FirebaseAuth auth;

    FirebaseUser user;

    String userId = null;

    String gameId = null;

    String hostId = null;

    DatabaseReference gameReference;
    DatabaseReference koZnaZnaReference;

    private Boolean leaveIntent = true;
    String playerValue = null;

    CountDownTimer questionTimer;

    private TextView questionTextView;
    private Button[] answerButtons;
    private int currentQuestionIndex = 0;
    private int player1Score = 0;
    private int player2Score = 0;
    private boolean questionOver = false;

    private final int NUMBER_OF_QUESTIONS = 6;

    private String currentCorrectAnswer = null;

    private ArrayList<Integer> randomNumbers;

    Dialog dialog;

    long millisecondsToFinishQuestion;
    TextView timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ko_zna_zna);

        gameId = getIntent().getStringExtra("GAME_ID");
        dialog = ProgressDialog.show(KoZnaZnaActivity.this, "",
                "Чекање противника да уђе у игру.", true);
        dialog.show();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                Toast.makeText(KoZnaZnaActivity.this, "Прошло је време за чекање играча.", Toast.LENGTH_SHORT).show();

            }
        };

        handler.postDelayed(runnable, 10000);




        gameReference= FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
        koZnaZnaReference = gameReference.child("koZnaZnaGame");
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();

        questionTextView = findViewById(R.id.questionTextView);
        answerButtons = new Button[4];
        answerButtons[0] = findViewById(R.id.answer1);
        answerButtons[1] = findViewById(R.id.answer2);
        answerButtons[2] = findViewById(R.id.answer3);
        answerButtons[3] = findViewById(R.id.answer4);
        timer = findViewById(R.id.timer);

        answerButtons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluateAnswer("1");
            }
        });
        answerButtons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluateAnswer("2");
            }
        });
        answerButtons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluateAnswer("3");
            }
        });
        answerButtons[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluateAnswer("4");
            }
        });


        gameReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    hostId = snapshot.child("player1").getValue(String.class);
                    if(hostId.equals(userId)){
                        gameReference.child("player1Location").setValue("KoZnaZna")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });


                        prepareGame();
                        determinePlayer("player1");

                        ValueEventListener locationListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String location = dataSnapshot.getValue(String.class);
                                    if(location.equals("KoZnaZna")){
                                        startGameListener();
                                        gameReference.child("player2Location").removeEventListener(this);

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };

                        listenerManager.addValueEventListener(gameReference.child("player2Location"),locationListener);


                    }
                    else{

                        gameReference.child("player2Location").setValue("KoZnaZna")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                        determinePlayer("player2");
                        ValueEventListener locationListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String location = dataSnapshot.getValue(String.class);
                                    if(location.equals("KoZnaZna")){
                                        preparedGameListener();
                                        gameReference.child("player1Location").removeEventListener(this);

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };

                        listenerManager.addValueEventListener(gameReference.child("player1Location"),locationListener);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void startGameListener(){
        ValueEventListener statusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue(String.class);
                    if(status.equals("start")){
                        dialog.dismiss();
                        handler.removeCallbacksAndMessages(null);
                        startGame();
                        koZnaZnaReference.child("status").removeEventListener(this);

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        listenerManager.addValueEventListener(koZnaZnaReference.child("status"),statusListener);

    }

    private void preparedGameListener(){

        ValueEventListener statusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue(String.class);
                    if(status.equals("gameprepared")){
                        readQuestions();
                        dialog.dismiss();
                        handler.removeCallbacksAndMessages(null);
                        koZnaZnaReference.child("status").setValue("start")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }else{
                                            startGame();
                                        }
                                    }
                                });
                        koZnaZnaReference.child("status").removeEventListener(this);

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        listenerManager.addValueEventListener(koZnaZnaReference.child("status"),statusListener);
    }


    private void prepareGame(){
        randomNumbers = generateRandomArray(1, NUMBER_OF_QUESTIONS, 5);


        StringBuilder questionsData = new StringBuilder();

        for (int i = 0; i < randomNumbers.size(); i++) {
            questionsData.append(randomNumbers.get(i));

            if (i < randomNumbers.size() - 1) {
                questionsData.append(", ");
            }
        }

        koZnaZnaReference.child("questions").setValue(questionsData.toString())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        koZnaZnaReference.child("player1points").setValue("0")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        koZnaZnaReference.child("player2points").setValue("0")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        koZnaZnaReference.child("status").setValue("gameprepared")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void readQuestions(){
        randomNumbers = new ArrayList<>();
        koZnaZnaReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    String input = snapshot.child("questions").getValue(String.class);

                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);
                            randomNumbers.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }


    public void startGame(){
        updateQuestionAndAnswers(randomNumbers);

    }

    private void updateQuestionAndAnswers(ArrayList<Integer> questionsNumbers) {

    if(questionTimer!=null){
        questionTimer.cancel();

    }

        restartButtons();

        evaluatedAnswerListener();

        if(questionsNumbers.isEmpty()){
            endGame();
            return;
        }

        DatabaseReference questionsReference = FirebaseDatabase.getInstance().getReference("users").child("koznazna_pitanja");
            String currentQuestionNumber = Integer.toString(questionsNumbers.get(0));
            questionsReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        String question = snapshot.child(currentQuestionNumber).child("tekst").getValue(String.class);
                        String answer1 = snapshot.child(currentQuestionNumber).child("1").getValue(String.class);
                        String answer2 = snapshot.child(currentQuestionNumber).child("2").getValue(String.class);
                        String answer3 = snapshot.child(currentQuestionNumber).child("3").getValue(String.class);
                        String answer4 = snapshot.child(currentQuestionNumber).child("4").getValue(String.class);
                        currentCorrectAnswer = snapshot.child(currentQuestionNumber).child("tacan").getValue(String.class);

                        questionTextView.setText(question);
                        answerButtons[0].setText(answer1);
                        answerButtons[1].setText(answer2);
                        answerButtons[2].setText(answer3);
                        answerButtons[3].setText(answer4);

                        randomNumbers.remove(0);

                        startCountdownTimer();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

    }


    private  void restartButtons(){
        for(int i = 0; i< answerButtons.length;i++){
            answerButtons[i].setClickable(true);
            answerButtons[i].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gameButtonColor)));
            answerButtons[i].setTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }


    private void restartChoices(){
        koZnaZnaReference.child(playerValue+"choice").setValue(null)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(KoZnaZnaActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void startCountdownTimer() {
       questionTimer = new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
                timer.setText(Long.toString(millisUntilFinished/1000));
            }

            public void onFinish() {
                    koZnaZnaReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                if(snapshot.child(playerValue+"choice").getValue(String.class) == null){
                                    evaluateAnswer("0");
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
            }
        }.start();
    }


    public void determinePlayer(String player){
        playerValue = player;
    }


    public void evaluatedAnswerListener(){
        ValueEventListener answerListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(snapshot.child("player1choice").getValue(String.class)!=null && !snapshot.child("player1choice").getValue(String.class).equals("0")){
                        String chosen = snapshot.child("player1choice").getValue(String.class);
                        answerButtons[Integer.parseInt(chosen)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(KoZnaZnaActivity.this, R.color.blue)));
                        answerButtons[Integer.parseInt(chosen)-1].setTextColor(ContextCompat.getColor(KoZnaZnaActivity.this, R.color.white));
                    }
                    if(snapshot.child("player2choice").getValue(String.class)!=null && !snapshot.child("player2choice").getValue(String.class).equals("0")){
                        String chosen = snapshot.child("player2choice").getValue(String.class);
                        answerButtons[Integer.parseInt(chosen)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(KoZnaZnaActivity.this, R.color.red)));
                        answerButtons[Integer.parseInt(chosen)-1].setTextColor(ContextCompat.getColor(KoZnaZnaActivity.this, R.color.white));
                    }
                    if(snapshot.child("player1choice").getValue(String.class)!=null && snapshot.child("player2choice").getValue(String.class)!=null){
                        questionTimer.cancel();
                        new CountDownTimer(2000, 1000) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {

                                showCorrectAnswer();

                            }
                        }.start();
                        koZnaZnaReference.removeEventListener(this);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
        listenerManager.addValueEventListener(koZnaZnaReference,answerListener);
    }

    private void evaluateAnswer(String selectedAnswer) {
        koZnaZnaReference.child(playerValue+"choice").setValue(selectedAnswer)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        for (int i = 0; i < answerButtons.length; i++) {
                            answerButtons[i].setClickable(false);
                        }




                        koZnaZnaReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if(snapshot.exists()){
                                    if(selectedAnswer.equals(currentCorrectAnswer)){
                                        if(playerValue.equals("player1") && (snapshot.child("player2choice").getValue(String.class)==null || !snapshot.child("player2choice").getValue(String.class).equals(currentCorrectAnswer))){
                                            int points = Integer.parseInt(snapshot.child("player1points").getValue(String.class));
                                            points += 2;
                                            koZnaZnaReference.child(playerValue+"points").setValue(Integer.toString(points)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                }
                                            });
                                        }else if(playerValue.equals("player2") && (snapshot.child("player1choice").getValue(String.class)==null || !snapshot.child("player1choice").getValue(String.class).equals(currentCorrectAnswer))){
                                            int points = Integer.parseInt(snapshot.child("player2points").getValue(String.class));
                                            points += 2;
                                            koZnaZnaReference.child(playerValue+"points").setValue(Integer.toString(points)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                }
                                            });
                                        }
                                    }else{
                                       if(!selectedAnswer.equals("0")){
                                           int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));
                                           points -= 2;
                                           koZnaZnaReference.child(playerValue+"points").setValue(Integer.toString(points)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                               @Override
                                               public void onComplete(@NonNull Task<Void> task) {

                                               }
                                           });
                                       }
                                    }

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });

                    }
                });

    }

    public static ArrayList<Integer> generateRandomArray(int min, int max, int count) {
        if (count > max - min + 1) {
            throw new IllegalArgumentException("Cannot generate more unique random numbers than available range.");
        }

        ArrayList<Integer> result = new ArrayList<>();
        ArrayList<Integer> availableNumbers = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            availableNumbers.add(i);
        }

        Random random = new Random();

        while (result.size() < count) {
            int randomIndex = random.nextInt(availableNumbers.size());
            int randomNumber = availableNumbers.get(randomIndex);
            availableNumbers.remove(randomIndex);
            result.add(randomNumber);
        }

        return result;
    }

    private void showCorrectAnswer(){

        answerButtons[Integer.parseInt(currentCorrectAnswer)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.backgroundColor)));
        answerButtons[Integer.parseInt(currentCorrectAnswer)-1].setTextColor(ContextCompat.getColor(this, R.color.black));

        restartChoices();


        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                updateQuestionAndAnswers(randomNumbers);

            }
        }.start();
    }

    private void endGame(){
        Toast.makeText(KoZnaZnaActivity.this, "Крај игре Ко зна зна.", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
        intent.putExtra("GAME_ID", gameId);
        intent.putExtra("NEXT_GAME", "Spojnice");
        leaveIntent = false;
        startActivity(intent);
        finish();
    }


    @Override
    protected void onDestroy() {

        if(gameId!=null && leaveIntent == true){
            final DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        String userId = user.getUid();

                        if(snapshot.child("player1").getValue().equals(userId)){
                            gameRef.child( "player1left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player2left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }else if(snapshot.child("player2").getValue().equals(userId)){
                            gameRef.child( "player2left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player1left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

                    String userId = user.getUid();
                    DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                    DatabaseReference activeUsers = usersRef.child("active");
                    activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });

        }
        super.onDestroy();
        if(questionTimer!=null){
            questionTimer.cancel();
        }
        handler.removeCallbacksAndMessages(null);
        listenerManager.removeAllListeners();
    }

}
