package com.example.slagalicaprojekat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.ChipDrawable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class FriendsActivity extends AppCompatActivity {

    private ImageButton backButton;
    private Button searchButton;
    private SearchView searchUsers;
    private LinearLayout playerListLayout;
    FirebaseAuth auth;
    FirebaseUser user;

    Dialog dialog;

    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();


    String currentGameId = null;

    private Boolean leaveIntent = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        backButton = findViewById(R.id.backButton);
        searchUsers = findViewById(R.id.searchUsers);
        playerListLayout = findViewById(R.id.playerListView);
        searchButton = findViewById(R.id.searchButton);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference currentUserRef = usersRef.child(userId);

        DatabaseReference friendsRef = currentUserRef.child("friends");

        friendsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot friendSnapshot : dataSnapshot.getChildren()) {
                    String friendKey = friendSnapshot.getKey();
                    String friendUserId = friendSnapshot.getValue(String.class);
                    addPlayer(friendUserId);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = searchUsers.getQuery().toString().trim();
                if (!query.isEmpty()) {
                    searchUsersByUsername(query);
                } else {
                    Toast.makeText(FriendsActivity.this, "Упишите корисничко име за претрагу", Toast.LENGTH_SHORT).show();
                }
            }
        });

        searchUsers.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchAndAddFriend(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
                leaveIntent = false;
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        addInvitationListener();
        gameInvitationListener();
    }



    public void gameInvitationListener(){
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        DatabaseReference currentUserRef = usersRef.child(userId);
        DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

        ValueEventListener gameEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot gameSnapshot : dataSnapshot.getChildren()){
                    if(currentGameId == null){
                        break;
                    }
                    else{
                        String gameId = gameSnapshot.getKey();
                        if(isDataValid(gameId)){
                            if(currentGameId.equals(gameId)){
                                if(dialog!=null){
                                    dialog.cancel();
                                }

                                return;

                            }
                        }else{
                            continue;
                        }
                    }
                }

                for (DataSnapshot gameSnapshot : dataSnapshot.getChildren()) {

                    String player1Id = gameSnapshot.child("player1").getValue(String.class);
                    String player2Id = gameSnapshot.child("player2").getValue(String.class);
                    String status = gameSnapshot.child("status").getValue(String.class);
                    String gameId = gameSnapshot.getKey();
                    currentGameId = gameId;

                    if(isDataValid(player1Id) && isDataValid(player2Id) && isDataValid(gameId) && isDataValid(status)){

                        if (player2Id.equals(userId) && status.equals("pending")) {

                            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference currentGame = gameRef.child("games").child(gameId);
                            AlertDialog.Builder builder = new AlertDialog.Builder(FriendsActivity.this);
                            builder.setTitle("Позив")
                                    .setMessage("Позвани сте у игру!")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {


                                        currentGame.child("status").setValue("accepted")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {

                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                                        intent.putExtra("IS_INVITED", true);
                                                        intent.putExtra("GAME_ID", gameId);
                                                        leaveIntent = false;
                                                        startActivity(intent);
                                                        finish();
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(FriendsActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {


                                        currentGame.child("status").setValue("declined")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(FriendsActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    });
                            dialog = builder.create();
                            dialog.show();


                            break;
                        }
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(FriendsActivity.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
            }
        };


        listenerManager.addValueEventListener(gamesReference, gameEventListener);
    }

    public Boolean isDataValid(String str){
        return str!=null;
    }

    private void addPlayer(String playerId) {
        View playerItemView = LayoutInflater.from(this).inflate(R.layout.player_item, null);
        ImageView profilePicture = playerItemView.findViewById(R.id.profilePicture);
        TextView username = playerItemView.findViewById(R.id.username);
        Button playButton = playerItemView.findViewById(R.id.playButton);

        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference currentUserRef = usersRef.child(playerId);

        currentUserRef.child("profileimg").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Object userImageRefObj = dataSnapshot.getValue();

                    if (userImageRefObj instanceof String) {
                        String userImageRef = (String) userImageRefObj;
                        Picasso.get().load(userImageRef).into(profilePicture);
                    } else {
                        Picasso.get().load(R.drawable.profile_picture).into(profilePicture);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        currentUserRef.child("username").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String usernameValue = dataSnapshot.getValue(String.class);
                    username.setText(usernameValue);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                intent.putExtra("FRIEND_ID",playerId);
                intent.putExtra("IS_FRIEND_REQUESTED", true);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

        playerListLayout.addView(playerItemView);
    }

    private void addFriend(String friendUserId) {

        DatabaseReference friendReference = FirebaseDatabase.getInstance().getReference("users").child(friendUserId);
        friendReference.child("invitations").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    long invCount = snapshot.getChildrenCount();
                    long nextInvNumber = invCount + 1;
                    for(DataSnapshot inv: snapshot.getChildren()){
                        if(inv.getValue(String.class).equals(user.getUid())){
                            Toast.makeText(FriendsActivity.this, "Захтев је већ послат!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    friendReference.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot Snapshot) {
                            if(Snapshot.exists()){
                                for(DataSnapshot friend: Snapshot.getChildren()){
                                    if(friend.getValue(String.class).equals(user.getUid())){
                                        Toast.makeText(FriendsActivity.this, "Тренутно сте пријатељи!", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                }

                                friendReference.child("invitations").child(String.valueOf(nextInvNumber)).setValue(user.getUid());
                                Toast.makeText(FriendsActivity.this, "Захтев успешно послат!", Toast.LENGTH_SHORT).show();

                            }else{
                                friendReference.child("invitations").child(String.valueOf(nextInvNumber)).setValue(user.getUid());
                                Toast.makeText(FriendsActivity.this, "Захтев успешно послат!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });


                }else {
                    friendReference.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot Snapshot) {
                            if(Snapshot.exists()){
                                for(DataSnapshot friend: Snapshot.getChildren()){
                                    if(friend.getValue(String.class).equals(user.getUid())){
                                        Toast.makeText(FriendsActivity.this, "Тренутно сте пријатељи!", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                }

                                friendReference.child("invitations").child("1").setValue(user.getUid());
                                Toast.makeText(FriendsActivity.this, "Захтев успешно послат!", Toast.LENGTH_SHORT).show();

                            }else{
                                friendReference.child("invitations").child("1").setValue(user.getUid());
                                Toast.makeText(FriendsActivity.this, "Захтев успешно послат!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    private void searchAndAddFriend(String username) {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        Query query = usersRef.orderByChild("username").equalTo(username);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    String foundUserId = userSnapshot.getKey();
                    if (!foundUserId.equals(user.getUid())) {
                        addFriend(foundUserId);
                    } else {
                        Toast.makeText(FriendsActivity.this, "Не можете додати себе за пријатеља.", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void searchUsersByUsername(String username) {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        Query query = usersRef.orderByChild("username").equalTo(username);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                playerListLayout.removeAllViews();

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    String foundUserId = userSnapshot.getKey();
                    if (!foundUserId.equals(user.getUid())) {
                        addSearchedPlayer(foundUserId);
                    } else {
                        Toast.makeText(FriendsActivity.this, "Не можете додати себе за пријатеља", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addSearchedPlayer(String userId) {
        View playerItemView = LayoutInflater.from(this).inflate(R.layout.friend_item, null);
        ImageView profilePicture = playerItemView.findViewById(R.id.profilePicture);
        TextView username = playerItemView.findViewById(R.id.username);
        Button addButton = playerItemView.findViewById(R.id.addButton);

        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users").child(userId);

        usersRef.child("profileimg").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String userImageRef = dataSnapshot.getValue(String.class);
                    Picasso.get().load(userImageRef).into(profilePicture);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        usersRef.child("username").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String usernameValue = dataSnapshot.getValue(String.class);
                    username.setText(usernameValue);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        addButton.setOnClickListener(v -> addFriend(userId));

        playerListLayout.addView(playerItemView);
    }


    private void addInvitationListener(){
        DatabaseReference currentUser = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations");

        ChildEventListener newInvListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String invId = snapshot.getValue(String.class);
                DatabaseReference currentUserRef = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
                DatabaseReference currentInv = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations").child(snapshot.getKey());
                DatabaseReference invUserRef = FirebaseDatabase.getInstance().getReference("users").child(invId);
                invUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            final String invUserName = snapshot.child("username").getValue(String.class);
                            AlertDialog.Builder builder = new AlertDialog.Builder(FriendsActivity.this);
                            builder.setTitle("Захтев")
                                    .setMessage("Корисник " + invUserName + " вам је послао захтев за пријатељство.")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {
                                        invUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(user.getUid())){
                                                            return;
                                                        }
                                                    }
                                                    invUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(user.getUid());
                                                    Toast.makeText(FriendsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    invUserRef.child("friends").child("1").setValue(user.getUid());
                                                    Toast.makeText(FriendsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        currentUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(invId)){
                                                            return;
                                                        }
                                                    }
                                                    currentUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(invId);
                                                    Toast.makeText(FriendsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    currentUserRef.child("friends").child("1").setValue(invId);
                                                    Toast.makeText(FriendsActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });
                                        currentInv.removeValue();


                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {
                                        currentInv.removeValue();
                                        Toast.makeText(FriendsActivity.this, "Захтев одбијен!", Toast.LENGTH_SHORT).show();


                                    });
                            dialog = builder.create();
                            dialog.show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addChildEventListener(currentUser, newInvListener);

    }


    @Override
    public void onBackPressed() {
        leaveIntent = false;
        Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
//        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }


    @Override
    protected void onDestroy() {

        if(leaveIntent == true){
            String userId = user.getUid();
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
            DatabaseReference activeUsers = usersRef.child("active");
            activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                }
            });

        }
        listenerManager.removeAllListeners();
        super.onDestroy();
    }


}