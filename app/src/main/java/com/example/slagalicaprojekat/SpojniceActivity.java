package com.example.slagalicaprojekat;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.renderscript.Sampler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.InternalHelpers;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class SpojniceActivity extends AppCompatActivity {


    public int activeQuestion;

    CountDownTimer countDownTimer;

    private Button[] leftButtons;
    private Button[] rightButtons;
    private ViewGroup rootLayout;
    private View overlayView;

    private final int NUMBER_OF_QUESTIONS = 3;

    private int numberOfTries;
    private Boolean isSecond;


    private Boolean leaveIntent = true;

    Handler handler = new Handler();
    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();

    private ArrayList<Integer> randomNumbers;

    private ArrayList<Integer> missed;

    FirebaseAuth auth;
    FirebaseUser user;
    String userId = null;
    String gameId = null;
    String hostId = null;
    DatabaseReference gameReference;
    DatabaseReference spojniceReference;
    String playerValue = null;
    Dialog dialog;
    private DatabaseReference databaseReference;

    private int attempts = 0 ;

    private int currentLeftButton = -1;


    private TextView questionText,timer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spojnice);

        gameId = getIntent().getStringExtra("GAME_ID");
        isSecond = getIntent().getBooleanExtra("SECOND_GAME",false);

        dialog = ProgressDialog.show(SpojniceActivity.this, "",
                "Чекање противника да уђе у игру.", true);
        dialog.show();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                Toast.makeText(SpojniceActivity.this, "Прошло је време за чекање играча.", Toast.LENGTH_SHORT).show();

            }
        };

        handler.postDelayed(runnable, 10000);

        timer = findViewById(R.id.timer);
        missed = new ArrayList<>();
        gameId = getIntent().getStringExtra("GAME_ID");

        gameReference= FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
        spojniceReference = gameReference.child("spojniceGame");
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();

        leftButtons = new Button[]{
                findViewById(R.id.Button1Left),
                findViewById(R.id.Button2Left),
                findViewById(R.id.Button3Left),
                findViewById(R.id.Button4Left),
                findViewById(R.id.Button5Left)
        };

        rightButtons = new Button[]{
                findViewById(R.id.Button1Right),
                findViewById(R.id.Button2Right),
                findViewById(R.id.Button3Right),
                findViewById(R.id.Button4Right),
                findViewById(R.id.Button5Right)
        };

        questionText = findViewById(R.id.questionTextView);


        gameReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    hostId = snapshot.child("player1").getValue(String.class);
                    if(hostId.equals(userId)){
                        gameReference.child("player1Location").setValue("Spojnice")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });


                        prepareGame();
                        determinePlayer("player1");

                        ValueEventListener locationListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String location = dataSnapshot.getValue(String.class);
                                    if(location.equals("Spojnice")){
                                        startGameListener();
                                        gameReference.child("player2Location").removeEventListener(this);

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };

                        listenerManager.addValueEventListener(gameReference.child("player2Location"),locationListener);


                    }
                    else{

                        gameReference.child("player2Location").setValue("Spojnice")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                        determinePlayer("player2");
                        ValueEventListener locationListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    String location = dataSnapshot.getValue(String.class);
                                    if(location.equals("Spojnice")){
                                        preparedGameListener();
                                        gameReference.child("player1Location").removeEventListener(this);

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };

                        listenerManager.addValueEventListener(gameReference.child("player1Location"),locationListener);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        setButtonsListeners();
        answerListeners();
    }


    private void setCurrentLeftButton(int val){
        currentLeftButton = val;
    }


    private void answerListeners(){

        ValueEventListener answerListeners = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot Snapshot) {
                if(Snapshot.exists()){
                    String currentPlayer = Snapshot.child("turn").getValue(String.class);

                    if(currentPlayer!=null&&playerValue!=null){

                        if(playerValue!=null&&playerValue.equals("player2")){
                            ValueEventListener changedPlayer1OpenListener = new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){


                                        List<Integer> opened = new ArrayList<>();

                                        String input = snapshot.getValue(String.class);

                                        List<String> numberStrings = new ArrayList<String>();

                                        if (input != null) {

                                            numberStrings = Arrays.asList(input.split(",\\s*"));

                                            for (String numberString : numberStrings) {
                                                try {
                                                    int number = 0;
                                                    number = Integer.parseInt(numberString);
                                                    opened.add(number);
                                                } catch (NumberFormatException e) {
                                                }
                                            }
                                        }

                                        if(!opened.isEmpty()){


                                            for(int i = 0; i<opened.size();i++){
                                                leftButtons[opened.get(i)-1].setClickable(false);
                                                leftButtons[opened.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.blue)));
                                                leftButtons[opened.get(i)-1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                                                final int num = opened.get(i)-1;

                                                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("spojnice").child(Integer.toString(activeQuestion));

                                                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                        DataSnapshot left = snapshot.child("DesnaKolona");

                                                        int correctAnswer = left.child(Integer.toString(num+1)).child("odgovor").getValue(Integer.class);
                                                        rightButtons[correctAnswer-1].setClickable(false);
                                                        rightButtons[correctAnswer-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.blue)));
                                                        rightButtons[correctAnswer-1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError error) {

                                                    }
                                                });

                                            }
                                        }



                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            };
                            listenerManager.addValueEventListener(spojniceReference.child("player1open"),changedPlayer1OpenListener);
                        } else if(playerValue!=null&&playerValue.equals("player1")){
                            ValueEventListener changedPlayer2OpenListener = new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot Snapshot) {
                                    if(Snapshot.exists()){

                                        List<Integer> opened = new ArrayList<>();

                                        String input = Snapshot.getValue(String.class);

                                        List<String> numberStrings;

                                        if (input != null) {
                                            numberStrings = Arrays.asList(input.split(",\\s*"));

                                            for (String numberString : numberStrings) {
                                                try {
                                                    int number = 0;
                                                    number = Integer.parseInt(numberString);
                                                    opened.add(number);
                                                } catch (NumberFormatException e) {
                                                }
                                            }
                                        }

                                        if(!opened.isEmpty()){
                                            for(int i = 0; i<opened.size();i++){

                                                leftButtons[opened.get(i)-1].setClickable(false);
                                                leftButtons[opened.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.red)));
                                                leftButtons[opened.get(i)-1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                                                final int num = opened.get(i)-1;

                                                DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("spojnice").child(Integer.toString(activeQuestion));

                                                questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                        DataSnapshot left = snapshot.child("DesnaKolona");

                                                        int correctAnswer = left.child(Integer.toString(num+1)).child("odgovor").getValue(Integer.class);
                                                        rightButtons[correctAnswer-1].setClickable(false);
                                                        rightButtons[correctAnswer-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.red)));
                                                        rightButtons[correctAnswer-1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError error) {

                                                    }
                                                });

                                            }
                                        }



                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            };
                            listenerManager.addValueEventListener(spojniceReference.child("player2open"),changedPlayer2OpenListener);
                        }

                        ValueEventListener unansweredListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot Snapshot) {
                                if(Snapshot.exists()){
                                        List<Integer> opened = new ArrayList<>();

                                        String input = Snapshot.getValue(String.class);

                                        List<String> numberStrings;

                                        if (input != null) {
                                            numberStrings = Arrays.asList(input.split(",\\s*"));

                                            for (String numberString : numberStrings) {
                                                try {
                                                    int number = 0;
                                                    number = Integer.parseInt(numberString);
                                                    opened.add(number);
                                                } catch (NumberFormatException e) {
                                                }
                                            }
                                        }

                                    if(!opened.isEmpty()){
                                        for(int i = 0; i<opened.size();i++){
                                            leftButtons[opened.get(i)-1].setClickable(false);
                                            leftButtons[opened.get(i)-1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.black)));
                                            leftButtons[opened.get(i)-1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        };

                        listenerManager.addValueEventListener(spojniceReference.child("unanswered"),unansweredListener);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(spojniceReference,answerListeners);

    }



    private void setButtonsListeners(){

        for (int i = 0; i < 5; i++) {
            final int num = i;

            rightButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentLeftButton == -1) {
                        Toast.makeText(SpojniceActivity.this, "Морате кликнути прво на појам са десне стране", Toast.LENGTH_SHORT).show();
                    }else{
                        checkAnswer(currentLeftButton,num);
                        setCurrentLeftButton(-1);
                    }
                }
            });

        }

        for (int i = 0; i < 5; i++) {
                final int num = i;
                leftButtons[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setCurrentLeftButton(num);
                        leftButtonController(num);
                    }
                });
        }
    }


    public void leftButtonController(int num){
        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    List<Integer> unguessed = new ArrayList<>();

                    unguessed.add(1);
                    unguessed.add(2);
                    unguessed.add(3);
                    unguessed.add(4);
                    unguessed.add(5);

                    String input = snapshot.child("player1open").getValue(String.class);

                    List<String> numberStrings = new ArrayList<String>();

                    if (input != null) {
                        numberStrings = Arrays.asList(input.split(",\\s*"));

                        for (String numberString : numberStrings) {
                            try {
                                int number = 0;
                                number = Integer.parseInt(numberString);
                                unguessed.remove(unguessed.indexOf(number));
                            } catch (NumberFormatException e) {
                            }
                        }
                    }

                    input = snapshot.child("player2open").getValue(String.class);


                    if (input != null) {
                        numberStrings = Arrays.asList(input.split(",\\s*"));

                        for (String numberString : numberStrings) {
                            try {
                                int number = 0;
                                number = Integer.parseInt(numberString);
                                unguessed.remove(unguessed.indexOf(number));
                            } catch (NumberFormatException e) {
                            }
                        }

                    }

                    input = snapshot.child("unanswered").getValue(String.class);


                    if (input != null) {
                        numberStrings = Arrays.asList(input.split(",\\s*"));

                        for (String numberString : numberStrings) {
                            try {
                                int number = 0;
                                number = Integer.parseInt(numberString);
                                unguessed.remove(unguessed.indexOf(number));
                            } catch (NumberFormatException e) {
                            }
                        }

                    }

                    if (!unguessed.isEmpty()) {
                        for (int i = 0; i < unguessed.size(); i++) {
                            leftButtons[unguessed.get(i) - 1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.gameButtonColor)));;
                            leftButtons[unguessed.get(i) - 1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.black));
                        }
                    }
                    if(playerValue.equals("player1")) {
                        leftButtons[num].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.blue)));
                        leftButtons[num].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));
                    }else if(playerValue.equals("player2")){
                        leftButtons[num].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.red)));
                        leftButtons[num].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void layoutOn(){
        rootLayout = findViewById(android.R.id.content);
        LayoutInflater inflater = LayoutInflater.from(this);
        overlayView = inflater.inflate(R.layout.overlay_layout, rootLayout, false);

        overlayView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        rootLayout.addView(overlayView);
    }

    public void endTurn(){
        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(playerValue.equals(snapshot.child("turn").getValue(String.class))&&playerValue.equals("player1")){
                        spojniceReference.child("turn").setValue("player2")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }else if(playerValue.equals(snapshot.child("turn").getValue(String.class))&&playerValue.equals("player2")){
                        spojniceReference.child("turn").setValue("player1")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }

                        Long numberOfTurn = snapshot.child("turnNumber").getValue(Long.class);

                        numberOfTurn++;

                        spojniceReference.child("turnNumber").setValue(numberOfTurn).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void layoutOff(){
        if (overlayView != null) {
            rootLayout.removeView(overlayView);
            overlayView = null;
        }
    }


    public void startTurn(){
        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String unanswered = snapshot.child("unanswered").getValue(String.class);


                    int count = 0;
                    if(unanswered!=null && unanswered.equals("")){
                        numberOfTries = 0;
                    }
                    else if(unanswered!=null) {

                        for(int i = 0;i<unanswered.length();i++){
                            if (unanswered.charAt(i) == ',') {
                                count++;
                            }
                        }
                        numberOfTries = count + 1;
                    }
                    else {
                        numberOfTries = 5;
                    }

                    clearUnanswered();


                    spojniceReference.child("unanswered").setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void clearUnanswered(){
        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {



                    String input = snapshot.child("unanswered").getValue(String.class);


                   if(input!=null){

                       List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                       for (String numberString : numberStrings) {
                           try {

                               int number = 0;
                               number= Integer.parseInt(numberString);
                               leftButtons[number - 1].setClickable(true);
                               leftButtons[number - 1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.gameButtonColor)));
                               leftButtons[number - 1].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.black));

                           } catch (NumberFormatException e) {
                           }
                       }
                   }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void checkTurn(){
        ValueEventListener checkTurn = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(snapshot.getValue(String.class)!=null){
                        if(playerValue.equals(snapshot.getValue(String.class))){
                            layoutOff();
                            startTurn();
                        }else{
                            layoutOn();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(spojniceReference.child("turn"),checkTurn);
    }


    public void isGameOver(){
        ValueEventListener gameOverListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String player1open = snapshot.child("player1open").getValue(String.class);
                    String player2open = snapshot.child("player2open").getValue(String.class);
                    Long numberOfTurn = snapshot.child("turnNumber").getValue(Long.class);
                    int count = 0;
                    if(player1open!=null&&!player1open.equals("")){
                        int p1count = 0;
                        for(int i = 0;i<player1open.length();i++){
                            if (player1open.charAt(i) == ',') {
                                p1count++;
                            }
                        }
                        if(p1count == 0){
                            count = 1;
                        } else count+=p1count+1;
                    }
                    if(player2open!=null&&!player2open.equals("")){
                        int p2count = 0;
                        for(int i = 0;i<player2open.length();i++){
                            if (player2open.charAt(i) == ',') {
                                p2count++;
                            }
                        }
                        if(p2count == 0){
                            count += 1;
                        } else count+=p2count+1;
                    }



                    if(count == 5 || numberOfTurn==3){
                        if(randomNumbers.size() == 1){
                            endGame();
                        }else{
                           nextGame();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addValueEventListener(spojniceReference,gameOverListener);
    }




    private Boolean checkAnswer(int currentActiveButton, int rightButton){
        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("spojnice").child(Integer.toString(activeQuestion));

        final Boolean[] answer = {false};

        final int leftNum = currentActiveButton;
        final int rightNum = rightButton;

        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    DataSnapshot left = snapshot.child("DesnaKolona");
                    DataSnapshot right = snapshot.child("LevaKolona");

                    int correctAnswer = left.child(Integer.toString(leftNum+1)).child("odgovor").getValue(Integer.class);

                    if(correctAnswer == rightNum+1){


                        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if(snapshot.exists()){
                                    String opened = snapshot.child(playerValue+"open").getValue(String.class);
                                    int count = 0;
                                    if(opened!=null){
                                        for(int i = 0;i<opened.length();i++){
                                            if (opened.charAt(i) == ',') {
                                                count++;
                                            }
                                        }
                                    }


                                    if(count==0 && (opened==null || opened.equals(""))){
                                        opened = Integer.toString(leftNum+1);
                                    } else {
                                        opened += ", "+Integer.toString(leftNum+1);
                                    }

                                    spojniceReference.child(playerValue+"open").setValue(opened).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (!task.isSuccessful()) {
                                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                                    if(playerValue.equals("player1")){
                                        leftButtons[leftNum].setClickable(false);
                                        leftButtons[leftNum].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.blue)));
                                        leftButtons[leftNum].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                                        rightButtons[rightNum].setClickable(false);
                                        rightButtons[rightNum].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.blue)));
                                        rightButtons[rightNum].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));
                                    }else if(playerValue.equals("player2")){
                                        leftButtons[leftNum].setClickable(false);
                                        leftButtons[leftNum].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.red)));
                                        leftButtons[leftNum].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                                        rightButtons[rightNum].setClickable(false);
                                        rightButtons[rightNum].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.red)));
                                        rightButtons[rightNum].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));
                                    }

                                    int points = Integer.parseInt(snapshot.child(playerValue+"points").getValue(String.class));
                                    points+=2;

                                    spojniceReference.child(playerValue+"points").setValue(Integer.toString(points)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (!task.isSuccessful()) {
                                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });



                                    answer[0] = true;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });


                    }else{
                        leftButtons[leftNum].setClickable(false);
                        leftButtons[leftNum].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(SpojniceActivity.this, R.color.black)));
                        leftButtons[leftNum].setTextColor(ContextCompat.getColor(SpojniceActivity.this, R.color.white));

                        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot Snapshot) {
                                String opened = Snapshot.child("unanswered").getValue(String.class);

                                int count = 0;
                                if(opened!=null){
                                    for(int i = 0;i<opened.length();i++){
                                        if (opened.charAt(i) == ',') {
                                            count++;
                                        }
                                    }
                                }


                                if(count==0 && (opened==null||opened.equals(""))){
                                    opened = Integer.toString(leftNum+1);
                                } else {
                                    opened += ", "+Integer.toString(leftNum+1);
                                }


                                spojniceReference.child("unanswered").setValue(opened).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if (!task.isSuccessful()) {
                                            Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        new CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
                     }
            public void onFinish() {
                numberOfTries--;
                if(numberOfTries == 0){
                    endTurn();
                }
            }
        }.start();

        return answer[0];
    }

    private void loadButtonsAndQuestion(){
        DatabaseReference questionReference = FirebaseDatabase.getInstance().getReference("users").child("spojnice").child(Integer.toString(activeQuestion));

        questionReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    DataSnapshot left = snapshot.child("DesnaKolona");
                    DataSnapshot right = snapshot.child("LevaKolona");

                    for(int i = 0; i<5;i++){
                        rightButtons[i].setText(right.child(Integer.toString(i+1)).getValue(String.class));
                        leftButtons[i].setText(left.child(Integer.toString(i+1)).child("tekst").getValue(String.class));
                    }

                    questionText.setText(snapshot.child("tekst").getValue(String.class));

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void startGame(){
        Toast.makeText(SpojniceActivity.this,"Игра почиње", Toast.LENGTH_SHORT).show();
        activeQuestion = randomNumbers.get(0);
        loadButtonsAndQuestion();
        checkTurn();
        isGameOver();
        countDownTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                timer.setText(Long.toString(millisUntilFinished/1000));
            }
            public void onFinish() {
                if(randomNumbers.size()==1) {
                    endGame();
                }else{
                    nextGame();
                }
            }
        }.start();
    }

    private void nextGame(){
        countDownTimer.cancel();
        listenerManager.removeAllListeners();
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {

                spojniceReference.child("player1open").setValue("")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                spojniceReference.child("player2open").setValue("")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                spojniceReference.child("unanswered").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });


                spojniceReference.child("turnNumber").setValue(1)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                spojniceReference.child("questions").setValue(Integer.toString(randomNumbers.get(1)))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });



                Intent intent = new Intent(getApplicationContext(), SpojniceActivity.class);
                intent.putExtra("GAME_ID", gameId);
                intent.putExtra("SECOND_GAME",true);
                intent.putExtra("GAME_NUMBER", randomNumbers.get(1));
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        }.start();

    }
    private void endGame(){
        listenerManager.removeAllListeners();
        countDownTimer.cancel();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {

                spojniceReference.child("player1open").setValue("")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                spojniceReference.child("player2open").setValue("")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                spojniceReference.child("unanswered").removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });


                spojniceReference.child("turnNumber").setValue(1)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                Toast.makeText(SpojniceActivity.this, "Крај игре Спојнице", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                intent.putExtra("GAME_ID", gameId);
                intent.putExtra("NEXT_GAME", "Asocijacije");
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        }.start();

    }


    private void readData(){
        randomNumbers = new ArrayList<>();
        spojniceReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    String input = snapshot.child("questions").getValue(String.class);

                    List<String> numberStrings = Arrays.asList(input.split(",\\s*"));

                    for (String numberString : numberStrings) {
                        try {
                            int number = 0;
                            number= Integer.parseInt(numberString);

                            randomNumbers.add(number);
                        } catch (NumberFormatException e) {
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void startGameListener(){
        ValueEventListener statusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue(String.class);
                    if(status.equals("start")){
                        dialog.dismiss();
                        handler.removeCallbacksAndMessages(null);
                        startGame();
                        spojniceReference.child("status").removeEventListener(this);

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        listenerManager.addValueEventListener(spojniceReference.child("status"),statusListener);

    }


    private void preparedGameListener(){

        ValueEventListener statusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String status = dataSnapshot.getValue(String.class);
                    if(status.equals("gameprepared")){
                        readData();
                        dialog.dismiss();
                        handler.removeCallbacksAndMessages(null);
                        spojniceReference.child("status").setValue("start")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                        }else{
                                            startGame();
                                        }
                                    }
                                });
                        spojniceReference.child("status").removeEventListener(this);

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };

        listenerManager.addValueEventListener(spojniceReference.child("status"),statusListener);
    }




    public static ArrayList<Integer> generateRandomArray(int min, int max, int count) {
        if (count > max - min + 1) {
            throw new IllegalArgumentException("Cannot generate more unique random numbers than available range.");
        }

        ArrayList<Integer> result = new ArrayList<>();
        ArrayList<Integer> availableNumbers = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            availableNumbers.add(i);
        }

        Random random = new Random();

        while (result.size() < count) {
            int randomIndex = random.nextInt(availableNumbers.size());
            int randomNumber = availableNumbers.get(randomIndex);
            availableNumbers.remove(randomIndex);
            result.add(randomNumber);
        }

        return result;
    }

    public void determinePlayer(String player){
        playerValue = player;
    }

    private void prepareGame() {

        if (isSecond && getIntent().getIntExtra("GAME_NUMBER", -1) != -1) {
            randomNumbers = new ArrayList<>();
            randomNumbers.add(getIntent().getIntExtra("GAME_NUMBER", -1));
            spojniceReference.child("status").setValue("gameprepared")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            spojniceReference.child("turn").setValue("player2")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            spojniceReference.child("turnNumber").setValue(1)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            spojniceReference.child("unanswered").setValue(null)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            randomNumbers = generateRandomArray(1, NUMBER_OF_QUESTIONS, 2);


            StringBuilder questionsData = new StringBuilder();

            for (int i = 0; i < randomNumbers.size(); i++) {
                questionsData.append(randomNumbers.get(i));

                if (i < randomNumbers.size() - 1) {
                    questionsData.append(", ");
                }
            }

            spojniceReference.child("questions").setValue(questionsData.toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            spojniceReference.child("player1points").setValue("0")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            spojniceReference.child("player2points").setValue("0")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            spojniceReference.child("status").setValue("gameprepared")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            spojniceReference.child("turn").setValue("player1")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            spojniceReference.child("turnNumber").setValue(1)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SpojniceActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    @Override
    protected void onDestroy() {

        if(gameId!=null && leaveIntent == true){
            final DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users").child("games").child(gameId);
            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists()){
                        String userId = user.getUid();

                        if(snapshot.child("player1").getValue().equals(userId)){
                            gameRef.child( "player1left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player2left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }else if(snapshot.child("player2").getValue().equals(userId)){
                            gameRef.child( "player2left").setValue(true);
                            gameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(snapshot.exists()){
                                        if(snapshot.child("player1left").getValue(Boolean.class)!=null){
                                            gameRef.setValue(null);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

                FirebaseAuth auth;
                FirebaseUser user;
                auth = FirebaseAuth.getInstance();
                user = auth.getCurrentUser();

                if(user!=null) {
                    String userId = user.getUid();
                    DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                    DatabaseReference activeUsers = usersRef.child("active");
                    activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

        }
        super.onDestroy();
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
        handler.removeCallbacksAndMessages(null);
        listenerManager.removeAllListeners();
    }

}
