package com.example.slagalicaprojekat;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;


public class RankActivity extends AppCompatActivity {

    private ImageButton backButton;

    private DatabaseReference usersRef;
    private ListView weeklyListView;
    private ListView monthlyListView;

    private String currentGameId = null;

    Dialog dialog;

    private FirebaseListenerManager listenerManager = new FirebaseListenerManager();

    private FirebaseAuth auth;

    private  FirebaseUser user;

    private Handler handler = new Handler();
    private Timer updateTimer = new Timer();

    private Boolean leaveIntent = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        backButton = findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RegistratedUserMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                leaveIntent = false;
                startActivity(intent);
                finish();
            }
        });

        usersRef = FirebaseDatabase.getInstance().getReference("users");

        weeklyListView = findViewById(R.id.weeklyListView);
        monthlyListView = findViewById(R.id.monthlyListView);

        loadRankLists();
        scheduleUpdates();
        gameInvitationListener();
        addInvitationListener();
    }

    private void addInvitationListener(){
        DatabaseReference currentUser = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations");

        ChildEventListener newInvListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String invId = snapshot.getValue(String.class);
                DatabaseReference currentUserRef = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
                DatabaseReference currentInv = FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).child("invitations").child(snapshot.getKey());
                DatabaseReference invUserRef = FirebaseDatabase.getInstance().getReference("users").child(invId);
                invUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            final String invUserName = snapshot.child("username").getValue(String.class);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RankActivity.this);
                            builder.setTitle("Захтев")
                                    .setMessage("Корисник " + invUserName + " вам је послао захтев за пријатељство.")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {
                                        invUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(user.getUid())){
                                                            return;
                                                        }
                                                    }
                                                    invUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(user.getUid());
                                                    Toast.makeText(RankActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    invUserRef.child("friends").child("1").setValue(user.getUid());
                                                    Toast.makeText(RankActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });

                                        currentUserRef.child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                if(snapshot.exists()){
                                                    long friendCount = snapshot.getChildrenCount();
                                                    long friendNextNumber = friendCount + 1;
                                                    for(DataSnapshot friend: snapshot.getChildren()){
                                                        if(friend.getValue(String.class).equals(invId)){
                                                            return;
                                                        }
                                                    }
                                                    currentUserRef.child("friends").child(String.valueOf(friendNextNumber)).setValue(invId);
                                                    Toast.makeText(RankActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();

                                                }else {
                                                    currentUserRef.child("friends").child("1").setValue(invId);
                                                    Toast.makeText(RankActivity.this, "Захтев прихваћен!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });
                                        currentInv.removeValue();


                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {
                                        currentInv.removeValue();
                                        Toast.makeText(RankActivity.this, "Захтев одбијен!", Toast.LENGTH_SHORT).show();


                                    });
                            dialog = builder.create();
                            dialog.show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listenerManager.addChildEventListener(currentUser, newInvListener);

    }


    private void scheduleUpdates() {
        TimerTask updateTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadRankLists();
                    }
                });
            }
        };
        updateTimer.schedule(updateTask, 0, 120000);
    }

    private void loadRankLists() {
        Query starsQuery = usersRef.orderByChild("stars").limitToLast(10);

        starsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void showRankList(List<User> users, ListView listView) {
        ArrayAdapter<User> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, users);
        listView.setAdapter(adapter);
    }

    private class StarsComparator implements Comparator<User> {
        @Override
        public int compare(User user1, User user2) {
            return Integer.compare(user2.getStars(), user1.getStars());
        }
    }
    public class User {
        private String userId;
        private int stars;

        public User() {

        }

        public String getUserId() {
            return userId;
        }

        public int getStars() {
            return stars;
        }
    }


    private void showRankDialog(int rank, int tokens) {
        String title = "Čestitamo!";
        String message = "Osvojili ste " + rank + ". mesto i dobili " + tokens + " tokena kao nagradu.";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private int getTokensForRank(int rank) {
        switch (rank) {
            case 1:
                return 7;
            case 2:
                return 5;
            case 3:
                return 3;
            default:
                return 1;
        }
    }


    public void gameInvitationListener(){
        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        DatabaseReference currentUserRef = usersRef.child(userId);
        DatabaseReference gamesReference = FirebaseDatabase.getInstance().getReference("users").child("games");

        ValueEventListener gameEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot gameSnapshot : dataSnapshot.getChildren()){
                    if(currentGameId == null){
                        break;
                    }
                    else{
                        String gameId = gameSnapshot.getKey();
                        if(isDataValid(gameId)){
                            if(currentGameId.equals(gameId)){
                                if(dialog!=null){
                                    dialog.cancel();
                                }

                                return;

                            }
                        }else{
                            continue;
                        }
                    }
                }

                for (DataSnapshot gameSnapshot : dataSnapshot.getChildren()) {

                    String player1Id = gameSnapshot.child("player1").getValue(String.class);
                    String player2Id = gameSnapshot.child("player2").getValue(String.class);
                    String status = gameSnapshot.child("status").getValue(String.class);
                    String gameId = gameSnapshot.getKey();
                    currentGameId = gameId;

                    if(isDataValid(player1Id) && isDataValid(player2Id) && isDataValid(gameId) && isDataValid(status)){

                        if (player2Id.equals(userId) && status.equals("pending")) {

                            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference("users");
                            DatabaseReference currentGame = gameRef.child("games").child(gameId);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RankActivity.this);
                            builder.setTitle("Позив")
                                    .setMessage("Позвани сте у игру!")
                                    .setPositiveButton("Прихвати", (dialog, which) -> {


                                        currentGame.child("status").setValue("accepted")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {

                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        Intent intent = new Intent(getApplicationContext(), GamesActivity.class);
                                                        intent.putExtra("IS_INVITED", true);
                                                        intent.putExtra("GAME_ID", gameId);
                                                        leaveIntent = false;
                                                        startActivity(intent);
                                                        finish();
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RankActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    })
                                    .setNegativeButton("Одбиј", (dialog, which) -> {


                                        currentGame.child("status").setValue("declined")
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (!task.isSuccessful()) {
                                                            Toast.makeText(RankActivity.this, R.string.errorWithSettingData, Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    });
                            dialog = builder.create();
                            dialog.show();


                            break;
                        }
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(RankActivity.this, R.string.databaseError, Toast.LENGTH_SHORT).show();
            }
        };


        listenerManager.addValueEventListener(gamesReference, gameEventListener);
    }

    public Boolean isDataValid(String str){
        return str!=null;
    }

    @Override
    public void onBackPressed() {
        leaveIntent = false;
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        super.onPause();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = user.getUid();
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        DatabaseReference activeUsers = usersRef.child("active");
        activeUsers.child(userId).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    protected void onDestroy() {

        if(leaveIntent == true){
            FirebaseAuth auth;
            FirebaseUser user;
            auth = FirebaseAuth.getInstance();
            user = auth.getCurrentUser();

            if(user!=null){
                String userId = user.getUid();
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
                DatabaseReference activeUsers = usersRef.child("active");
                activeUsers.child(userId).setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
            }

        }
        listenerManager.removeAllListeners();
        super.onDestroy();
    }

}